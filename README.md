## Description
[![Project Status: Abandoned – Initial development has started, but there has not yet been a stable, usable release; the project has been abandoned and the author(s) do not intend on continuing development.](https://www.repostatus.org/badges/latest/abandoned.svg)](https://www.repostatus.org/#abandoned)
[![license: GPL-3.0-or-later](https://img.shields.io/badge/license-GPL--3.0--or--later-blueviolet)](https://www.gnu.org/licenses/gpl-3.0.html)

[Link to the master thesis](https://elocus.lib.uoc.gr/search/?search_type=simple&search_help=&display_mode=overview&wf_step=init&show_hidden=0&number=10&keep_number=&cclterm1=%CF%80%CE%B1%CF%80%CE%B1%CE%B4%CF%8C%CF%80%CE%BF%CF%85%CE%BB%CE%BF%CF%82&cclterm2=&cclterm3=&cclterm4=&cclterm5=&cclterm6=&cclterm7=&cclterm8=&cclterm9=&cclfield1=term&cclfield2=&cclfield3=&cclfield4=&cclfield5=&cclfield6=&cclfield7=&cclfield8=&cclfield9=&cclop1=&cclop2=&cclop3=&cclop4=&cclop5=&cclop6=&cclop7=&cclop8=&display_help=0&offset=1&search_coll[metadata]=0&search_coll[dlib]=1&&stored_cclquery=&skin=&rss=0&store_query=1&show_form=&clone_file=&export_method=none&display_mode=detail&offset=3&number=1&keep_number=10&old_offset=1&search_help=detail)

This project explores the computational advantages that non-monotone dendritic
activation functions provide to pyramidal neurons, compared to monotone functions.

The dCaAP (Gidon et. al, 2020) spiking mechanism was modeled, and the
computational capacity of a CL2/3 pyramidal neuron was assessed, and also 
compared to that of a pyramidal neuron whose denrites implement a sigmoid input-
output function (Poirazi, 2003).

The comparison has been realized in terms of Boolean functions implementation
(Caze et. al, 2013; Tran-Van-Minh et. al, 2015).
