from mpl_toolkits.mplot3d import Axes3D                                         # needed for 3D scatterplot

import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

# local import
from data_norm import data_norm_sigmoid

################################################################################
# Cluster and local data paths
save_path = "/home/cluster/sotirisp/all_bfs/data/all_sigmoid"
save_path_norm = "/home/cluster/sotirisp/all_bfs/data/all_sigmoid_norm"

M, order, sigmoid_center = data_norm_sigmoid(save_path_norm)
print("loaded data!")

# NOTE:
# If working with python3, 'byte' coding of strings saved in python2 is
# misinterpreted; if this is the case, check for the 'b' that precedes all
# these strings
#if sys.version_info[0] == 3:
#	order = np.char.decode(order,'UTF-8')

# Find the index to exclude (the FALSE Boolean Function) later from Figure 1
to_delete = []
for index, name in enumerate(order):
	if order[index] == "FALSE" or order[index] == "XOR" or order[index] == "XNOR":
		to_delete.append(index)


###############################################################################
# Initialize plots

# Common labels for x,y,z axes and fontsize for axes and titles
x_l = "$w_{11}$"
y_l = "$w_{12}$"
z_l = "$b_{1}$"
fs = 8

# Common limits and ticks for all axes
w_min_limit = -1.0
w_max_limit = 1.0
b_min_limit = -1.0
b_max_limit = 1.0
step = 1.0

# Common camera angles for all axes
el = 20.0
az = 62.0

# Create a figure that plots solutions of all 12 Boolean functions in the
# same space (exclude False for visualization purposes)
# Sup_Figure_2
fig1 = plt.figure()

ax1 = fig1.add_subplot(111,projection='3d')
ax1.set_xlabel(x_l,fontsize=fs)
ax1.set_ylabel(y_l,fontsize=fs)
ax1.set_zlabel(z_l,fontsize=fs)
ax1.set_title("Parameter space of all BFs solutions",fontsize=fs)
ax1.set_xlim(w_min_limit, w_max_limit)
ax1.set_ylim(w_min_limit, w_max_limit)
ax1.set_zlim(b_min_limit, b_max_limit)
ax1.set_xticks(np.arange(w_min_limit,w_max_limit+1,step))
ax1.set_yticks(np.arange(w_min_limit,w_max_limit+1,step))
ax1.set_zticks(np.arange(b_min_limit,b_max_limit+1,step))
ax1.view_init(elev=el, azim=az)
                
# Create a figure with 12 subplots; each subplot corresponds to the clustered
# solutions of each Boolean function
# Sup_Figure_2X
fig2, axes2 = plt.subplots(nrows=3,ncols=4,subplot_kw=dict(projection='3d'))
fig2.suptitle("All Boolean Functions",fontsize=fs)

# Create a figure with 4 subplots; each subplot corresponds to the clustered
# solutions of each Boolean function (and, nand, or, nor)
# Sup_Figure_3
fig3, axes3 = plt.subplots(nrows=2,ncols=2,subplot_kw=dict(projection='3d'))
fig3.suptitle("AND, NAND, OR, NOR",fontsize=fs)

# Create a figure that plots the solutions of XOR
# Figure_1A
fig4 = plt.figure()

ax4 = fig4.add_subplot(111,projection='3d')
ax4.set_xlabel(x_l,fontsize=fs)
ax4.set_ylabel(y_l,fontsize=fs)
ax4.set_zlabel(z_l,fontsize=fs)
ax4.set_title("Parameter space of XOR solutions",fontsize=fs)
ax4.set_xlim(w_min_limit, w_max_limit)
ax4.set_ylim(w_min_limit, w_max_limit)
ax4.set_zlim(b_min_limit, b_max_limit)
ax4.set_xticks(np.arange(w_min_limit,w_max_limit+1,step))
ax4.set_yticks(np.arange(w_min_limit,w_max_limit+1,step))
ax4.set_zticks(np.arange(b_min_limit,b_max_limit+1,step))
ax4.view_init(elev=el, azim=az)
            
# Create a figure that plots the solutions of XNOR
# Figure_1B
fig5 = plt.figure()

ax5 = fig5.add_subplot(111,projection='3d')
ax5.set_xlabel(x_l,fontsize=fs)
ax5.set_ylabel(y_l,fontsize=fs)
ax5.set_zlabel(z_l,fontsize=fs)
ax5.set_title("Parameter space of XNOR solutions",fontsize=fs)
ax5.set_xlim(w_min_limit, w_max_limit)
ax5.set_ylim(w_min_limit, w_max_limit)
ax5.set_zlim(b_min_limit, b_max_limit)
ax5.set_xticks(np.arange(w_min_limit,w_max_limit+1,step))
ax5.set_yticks(np.arange(w_min_limit,w_max_limit+1,step))
ax5.set_zticks(np.arange(b_min_limit,b_max_limit+1,step))
ax5.view_init(elev=el, azim=az)

# Create yet another figure with 4 subplots; each subplot corresponds to the
# clustered solutions of each Boolean function (all, xor, xnor, null)
# Sup_Figure_X
fig6, axes6 = plt.subplots(nrows=2,ncols=2,subplot_kw=dict(projection='3d'))
fig6.suptitle("ALL, XOR, XNOR",fontsize=fs)

# Colormap Figure
fig7 = plt.figure()
ax7 = fig7.add_subplot(111)
# null linspace, what we really want to keep is the legend
z = np.linspace(13,1,13)
v = np.array([1])
# add "No solution" to order
new_order = list(order)
new_order.append("No solution")

################################################################################
# Control specific matplotlib aspects for proper data plotting

# Define light gray as the color for noise (not grouped in clusters) and the
# rest of the clusters as a spectrum for better visualization
n_color = [0.25, 0.25, 0.25, 0.4]

# Define a custom, qualitative,'Paired'-like colormap, to be used for
# clustering
# (lightskyblue, dodgerblue, lightgreen, forestgreen, lightcoral, myred,
# sandybrown, darkorange, mediumpurple, indigo, banana, mybrown)
# last color is gray and is used for visualization purposes in Colormap Figure (7)
colors = [ [0.5, 0.8, 1.0, 1.0], [0.1, 0.5, 1.0, 1.0], [0.5, 0.9, 0.5, 1.0],
			[0.1, 0.5, 0.1, 1.0], [0.9, 0.5, 0.5, 1.0], [0.8, 0.2, 0.1, 1.0],
			[1.0, 0.6, 0.4, 1.0], [1.0, 0.5, 0.0, 1.0], [0.5, 0.4, 0.8, 1.0],
			[0.3, 0.0, 0.5, 1.0], [1.0, 1.0, 0.4, 1.0], [0.6, 0.3, 0.1, 1.0],
			[0.5, 0.5, 0.5, 1.0] ]


################################################################################
# Evaluate each Boolean Function separately
for bf in range(0,12):
	# keep the 100% accuracy combinations of each Boolean function
	acc100 = M[:,-1,bf]==100
	# verify that solutions exist; if not move on to the next Boolean function
	if not any(acc100):
		continue
	# else keep the solutions of each Boolean Function
	Mnew = M[:,0:3,bf][acc100]
	
	# Round the values of matrix Mnew to 2 decimals and keep unique solutions
	Mr = np.around(Mnew,decimals=2)
	Mnew = np.unique(Mr,axis=0)
	
	
################################################################################
	# Plot the final data
	
	# Fill in Figure 2
	# Control the correct subplot
	# Define in which column each subplot should be positioned
	if (bf==0 or bf==4 or bf==8):
		j = 0
	elif (bf==1 or bf==5 or bf==9):
		j = 1
	elif (bf==2 or bf==6 or bf==10):
		j = 2
	else:
		j = 3

	# move horizontally for filling the subplots
	ax2 = axes2[0+bf//4,0+j]

	ax2.set_xlabel(x_l,fontsize=fs)
	ax2.set_ylabel(y_l,fontsize=fs)
	ax2.set_zlabel(z_l,fontsize=fs)
	ax2.set_title(str(order[bf]),fontsize=fs)
	ax2.set_xlim(w_min_limit, w_max_limit)
	ax2.set_ylim(w_min_limit, w_max_limit)
	ax2.set_zlim(b_min_limit, b_max_limit)
	ax2.set_xticks(np.arange(w_min_limit,w_max_limit+1,step))
	ax2.set_yticks(np.arange(w_min_limit,w_max_limit+1,step))
	ax2.set_zticks(np.arange(b_min_limit,b_max_limit+1,step))
	ax2.view_init(elev=el, azim=az)
	# remove grids, make background transparent
	ax2.grid(b=None)
	ax2.xaxis.pane.fill = False
	ax2.yaxis.pane.fill = False
	ax2.zaxis.pane.fill = False

	ax2.scatter(Mnew[:,0],Mnew[:,1],Mnew[:,2],c=[colors[bf]])
	
	# keep each (sub)matrix without the False Boolean function
	if order[bf] == "FALSE":
		continue

	# Fill in figure 1 (exclude False for visualization purposes)
	ax1.scatter(Mnew[:,0],Mnew[:,1],Mnew[:,2],c=[colors[bf]])
	# remove grids, make background transparent
	ax1.grid(b=None)
	ax1.xaxis.pane.fill = False
	ax1.yaxis.pane.fill = False
	ax1.zaxis.pane.fill = False
	
	# Fill in figure 4 (XOR)
	if order[bf] == "XOR":
		ax4.scatter(Mnew[:,0],Mnew[:,1],Mnew[:,2],c=[colors[bf]])

	ax4.grid(b=None)
	ax4.xaxis.pane.fill = False
	ax4.yaxis.pane.fill = False
	ax4.zaxis.pane.fill = False
	
	# Fill in figure 5 (XNOR)
	if order[bf] == "XNOR":
		ax5.scatter(Mnew[:,0],Mnew[:,1],Mnew[:,2],c=[colors[bf]])

	ax5.grid(b=None)
	ax5.xaxis.pane.fill = False
	ax5.yaxis.pane.fill = False
	ax5.zaxis.pane.fill = False
	
	# Fill in Figure 3
	# first row (and, nand)
	if order[bf] == "AND":
		ax3 = axes3[0,0]
		
		ax3.set_xlabel(x_l,fontsize=fs)
		ax3.set_ylabel(y_l,fontsize=fs)
		ax3.set_zlabel(z_l,fontsize=fs)
		ax3.set_title(str(order[bf]),fontsize=fs)
		ax3.set_xlim(w_min_limit, w_max_limit)
		ax3.set_ylim(w_min_limit, w_max_limit)
		ax3.set_zlim(b_min_limit, b_max_limit)
		ax3.set_xticks(np.arange(w_min_limit,w_max_limit+1,step))
		ax3.set_yticks(np.arange(w_min_limit,w_max_limit+1,step))
		ax3.set_zticks(np.arange(b_min_limit,b_max_limit+1,step))
		ax3.view_init(elev=el, azim=az)
		
		ax3.grid(b=None)
		ax3.xaxis.pane.fill = False
		ax3.yaxis.pane.fill = False
		ax3.zaxis.pane.fill = False

		ax3.scatter(Mnew[:,0],Mnew[:,1],Mnew[:,2],c=[colors[bf]])
		
	elif order[bf] == "NAND":
		ax3 = axes3[0,1]
		
		ax3.set_xlabel(x_l,fontsize=fs)
		ax3.set_ylabel(y_l,fontsize=fs)
		ax3.set_zlabel(z_l,fontsize=fs)
		ax3.set_title(str(order[bf]),fontsize=fs)
		ax3.set_xlim(w_min_limit, w_max_limit)
		ax3.set_ylim(w_min_limit, w_max_limit)
		ax3.set_zlim(b_min_limit, b_max_limit)
		ax3.set_xticks(np.arange(w_min_limit,w_max_limit+1,step))
		ax3.set_yticks(np.arange(w_min_limit,w_max_limit+1,step))
		ax3.set_zticks(np.arange(b_min_limit,b_max_limit+1,step))
		ax3.view_init(elev=el, azim=az)
		
		ax3.grid(b=None)
		ax3.xaxis.pane.fill = False
		ax3.yaxis.pane.fill = False
		ax3.zaxis.pane.fill = False

		ax3.scatter(Mnew[:,0],Mnew[:,1],Mnew[:,2],c=[colors[bf]])
		
	# second row (or, nor)
	elif order[bf] == "OR":
		ax3 = axes3[1,0]
		
		ax3.set_xlabel(x_l,fontsize=fs)
		ax3.set_ylabel(y_l,fontsize=fs)
		ax3.set_zlabel(z_l,fontsize=fs)
		ax3.set_title(str(order[bf]),fontsize=fs)
		ax3.set_xlim(w_min_limit, w_max_limit)
		ax3.set_ylim(w_min_limit, w_max_limit)
		ax3.set_zlim(b_min_limit, b_max_limit)
		ax3.set_xticks(np.arange(w_min_limit,w_max_limit+1,step))
		ax3.set_yticks(np.arange(w_min_limit,w_max_limit+1,step))
		ax3.set_zticks(np.arange(b_min_limit,b_max_limit+1,step))
		ax3.view_init(elev=el, azim=az)
		
		ax3.grid(b=None)
		ax3.xaxis.pane.fill = False
		ax3.yaxis.pane.fill = False
		ax3.zaxis.pane.fill = False

		ax3.scatter(Mnew[:,0],Mnew[:,1],Mnew[:,2],c=[colors[bf]])

	elif order[bf] == "NOR":
		ax3 = axes3[1,1]
		
		ax3.set_xlabel(x_l,fontsize=fs)
		ax3.set_ylabel(y_l,fontsize=fs)
		ax3.set_zlabel(z_l,fontsize=fs)
		ax3.set_title(str(order[bf]),fontsize=fs)
		ax3.set_xlim(w_min_limit, w_max_limit)
		ax3.set_ylim(w_min_limit, w_max_limit)
		ax3.set_zlim(b_min_limit, b_max_limit)
		ax3.set_xticks(np.arange(w_min_limit,w_max_limit+1,step))
		ax3.set_yticks(np.arange(w_min_limit,w_max_limit+1,step))
		ax3.set_zticks(np.arange(b_min_limit,b_max_limit+1,step))
		ax3.view_init(elev=el, azim=az)
		
		ax3.grid(b=None)
		ax3.xaxis.pane.fill = False
		ax3.yaxis.pane.fill = False
		ax3.zaxis.pane.fill = False

		ax3.scatter(Mnew[:,0],Mnew[:,1],Mnew[:,2],c=[colors[bf]])
		
	# Fill in Figure 6
	# first row (always plot in first subplot)
	ax6 = axes6[0,0]
	ax6.scatter(Mnew[:,0],Mnew[:,1],Mnew[:,2],c=[colors[bf]])
	# second row (xor, xnor)
	if order[bf] == "XOR":
		ax6 = axes6[1,0]
		
		ax6.set_xlabel(x_l,fontsize=fs)
		ax6.set_ylabel(y_l,fontsize=fs)
		ax6.set_zlabel(z_l,fontsize=fs)
		ax6.set_title(str(order[bf]),fontsize=fs)
		ax6.view_init(elev=el, azim=az)
		
		ax6.grid(b=None)
		ax6.xaxis.pane.fill = False
		ax6.yaxis.pane.fill = False
		ax6.zaxis.pane.fill = False

		ax6.scatter(Mnew[:,0],Mnew[:,1],Mnew[:,2],c=[colors[bf]])
		
	elif order[bf] == "XNOR":
		ax6 = axes6[1,1]

		ax6.set_xlabel(x_l,fontsize=fs)
		ax6.set_ylabel(y_l,fontsize=fs)
		ax6.set_zlabel(z_l,fontsize=fs)
		ax6.set_title(str(order[bf]),fontsize=fs)
		ax6.view_init(elev=el, azim=az)
		
		ax6.grid(b=None)
		ax6.xaxis.pane.fill = False
		ax6.yaxis.pane.fill = False
		ax6.zaxis.pane.fill = False

		ax6.scatter(Mnew[:,0],Mnew[:,1],Mnew[:,2],c=[colors[bf]])

	ax6.set_xlim(w_min_limit, w_max_limit)
	ax6.set_ylim(w_min_limit, w_max_limit)
	ax6.set_zlim(b_min_limit, b_max_limit)
	ax6.set_xticks(np.arange(w_min_limit,w_max_limit+1,step))
	ax6.set_yticks(np.arange(w_min_limit,w_max_limit+1,step))
	ax6.set_zticks(np.arange(b_min_limit,b_max_limit+1,step))
	ax6.view_init(elev=el, azim=az)
	
	ax6.grid(b=None)
	ax6.xaxis.pane.fill = False
	ax6.yaxis.pane.fill = False
	ax6.zaxis.pane.fill = False
	
# Fill in figure 7
for i in range(len(colors)):
	ax7.scatter(v,z[i],c=[colors[i]])
# remove grids, make background transparent
ax7.grid(False)
ax7.legend(new_order,title='Boolean Functions',loc='center right',frameon=False,
			bbox_to_anchor=(1.0, 0.6),fontsize="small",scatterpoints=1)
			
# add legend and show plots
ax1.legend(np.delete(order,to_delete),title='Boolean Functions',loc='center right',
			frameon=False,bbox_to_anchor=(1.0, 0.6),fontsize="small",
			scatterpoints=1)

plt.tight_layout()

plt.show()
