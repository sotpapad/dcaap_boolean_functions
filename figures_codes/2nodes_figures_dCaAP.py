from mpl_toolkits.mplot3d import Axes3D                                         # needed for 3D scatterplot

import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

# local import
from data_norm_2nodes import data_norm_dCaAP

################################################################################
# Cluster data paths
names = ["XOR", "XNOR", "AND", "NAND", "OR", "NOR"]

raw_data = ["/home/cluster/sotirisp/all_bfs/data/2nodes_xor",
			"/home/cluster/sotirisp/all_bfs/data/2nodes_xnor",
			"/home/cluster/sotirisp/all_bfs/data/2nodes_and",
			"/home/cluster/sotirisp/all_bfs/data/2nodes_nand",
			"/home/cluster/sotirisp/all_bfs/data/2nodes_or",
			"/home/cluster/sotirisp/all_bfs/data/2nodes_nor"]

norm_data = ["/home/cluster/sotirisp/all_bfs/data/2nodes_xor_norm",
			 "/home/cluster/sotirisp/all_bfs/data/2nodes_xnor_norm",
			 "/home/cluster/sotirisp/all_bfs/data/2nodes_and_norm",
			 "/home/cluster/sotirisp/all_bfs/data/2nodes_nand_norm",
			 "/home/cluster/sotirisp/all_bfs/data/2nodes_or_norm",
			 "/home/cluster/sotirisp/all_bfs/data/2nodes_nor_norm"]

# Choose Boolean Function to operate on (0-5, order as in "names")
bf = 0
save_path = raw_data[bf]

M, dCaAP_center_min, dCaAP_center_max = data_norm_dCaAP(save_path)
print("loaded data!")

###############################################################################
# Initialize plots

# Common labels for x,y,z axes and fontsize for axes and titles
x_l = "$w_{11}$"
y_l = "$w_{12}$"
z_l = "$b_{1}$"
bar_l = "$b_{1}$ - $b_{2}$"
fs = 8

# Common limits and ticks for all axes
w_min_limit = -1.0
w_max_limit = 1.0
b_min_limit = -1.0
b_max_limit = 1.0
step = 1.0

# Common camera angles for all axes
el = 20.0
az = 62.0

# Solutions Figure
fig1 = plt.figure()

ax1 = fig1.add_subplot(111,projection='3d')
ax1.set_xlabel(x_l,fontsize=fs)
ax1.set_ylabel(y_l,fontsize=fs)
ax1.set_zlabel(z_l,fontsize=fs)
ax1.set_title("Parameter space of " + names[bf] + " solutions",fontsize=fs)
ax1.set_xlim(w_min_limit, w_max_limit)
ax1.set_ylim(w_min_limit, w_max_limit)
ax1.set_zlim(b_min_limit, b_max_limit)
ax1.set_xticks(np.arange(w_min_limit,w_max_limit+1,step))
ax1.set_yticks(np.arange(w_min_limit,w_max_limit+1,step))
ax1.set_zticks(np.arange(b_min_limit,b_max_limit+1,step))
ax1.view_init(elev=el, azim=az)

ax1.grid(b=None)
ax1.xaxis.pane.fill = False
ax1.yaxis.pane.fill = False
ax1.zaxis.pane.fill = False

p1 = ax1.scatter(M[:,0],M[:,1],M[:,2],c=(M[:,2] - M[:,3]), cmap=mpl.cm.cool)

# Control the position and size of the bargraph
#c1ax = plt.axes([0.1,0.2,0.1,0.8])												# x,y,thickness,h
cbar1 = plt.colorbar(p1)#,cax=c1ax)    
cbar1.set_label(bar_l)
