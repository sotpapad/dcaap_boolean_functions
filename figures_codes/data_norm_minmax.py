import numpy as np

def data_norm(savepath):
	""" Function that normalizes the data and saves them in a matrix or if such
		data already exist, justs loads and returns the arrays. """
	
	# if not normalized data are loaded perform normalization
	if savepath == "/home/cluster/sotirisp/all_bfs/data/all":
		# Load the saved variables
		data = np.load(savepath + ".npz")
		order = data["bf_order"]
		M = data["sols_matrix"]
		
		# find the minimum and maximum values in the data, considering only the 
		# parameters that actually compute each particular Boolean Function
		mxs = []
		mns = []
		for bf in range(0,12):
			acc100 = M[:,-1,bf]==100
			M100 = M[:,0:3,bf][acc100]
			mx = np.max(M100[:,0:3])
			mn = np.min(M100[:,0:3])
			mxs.append(mx)
			mns.append(mn)

		# normalize the data with the respect to the max value of the 3 variables
		# we are interested in (w11,w12,b1), using min-max scaling in the range
		# (-1,1)
		mx = np.max(mxs)
		mn = np.min(mns)
		print(mx,mn)

		M[:,0:3,:] = (-1.0) + ( M[:,0:3,:] - mn ) * ( 1.0 - (-1.0) ) / ( mx - mn )

		# also normalize the range within which the dCaAP outputs 1
		dCaAP_center_min = (-1.0) + (0.47 - mn) * (1.0 - (-1.0) ) / (mx - mn)
		dCaAP_center_max = (-1.0) + (0.7 - mn) * (1.0 - (-1.0) ) / (mx - mn)
		
		# save the normalized data, so that the procedure doesn't have to be
		# repeated every time
		np.savez_compressed("/home/cluster/sotirisp/all_bfs/data/all_norm",
							sols_matrix=M, bf_order=order,
							dCaAP_center_min=dCaAP_center_min,
							dCaAP_center_max=dCaAP_center_max)
		
		# save XOR separately for debugging
		np.savez_compressed("/home/cluster/sotirisp/all_bfs/data/xor_norm",
							sols_matrix=M[:,:,0], bf_order=order,
							dCaAP_center_min=dCaAP_center_min,
							dCaAP_center_max=dCaAP_center_max)
	
	# if normalized data are loaded just return the arrays
	elif savepath == "/home/cluster/sotirisp/all_bfs/data/all_norm":
		# Load the saved variables
		data = np.load(savepath + ".npz")
		order = data["bf_order"]
		M = data["sols_matrix"]
		dCaAP_center_min = data["dCaAP_center_min"]
		dCaAP_center_max = data["dCaAP_center_max"]
		
	# if debugging only load xor
	elif savepath == "/home/cluster/sotirisp/all_bfs/data/xor_norm":
		# Load the saved variables
		data = np.load(savepath + ".npz")
		order = data["bf_order"]
		M = data["sols_matrix"]
		dCaAP_center_min = data["dCaAP_center_min"]
		dCaAP_center_max = data["dCaAP_center_max"]
	
	return M, order, dCaAP_center_min, dCaAP_center_max
