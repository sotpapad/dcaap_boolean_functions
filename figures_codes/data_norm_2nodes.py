import numpy as np

raw_data = ["/home/cluster/sotirisp/all_bfs/data/2nodes_xor",
			"/home/cluster/sotirisp/all_bfs/data/2nodes_xnor",
			"/home/cluster/sotirisp/all_bfs/data/2nodes_and",
			"/home/cluster/sotirisp/all_bfs/data/2nodes_nand",
			"/home/cluster/sotirisp/all_bfs/data/2nodes_or",
			"/home/cluster/sotirisp/all_bfs/data/2nodes_nor"]

norm_data = ["/home/cluster/sotirisp/all_bfs/data/2nodes_xor_norm",
			"/home/cluster/sotirisp/all_bfs/data/2nodes_xnor_norm",
			"/home/cluster/sotirisp/all_bfs/data/2nodes_and_norm",
			"/home/cluster/sotirisp/all_bfs/data/2nodes_nand_norm",
			"/home/cluster/sotirisp/all_bfs/data/2nodes_or_norm",
			"/home/cluster/sotirisp/all_bfs/data/2nodes_nor_norm"]

def data_norm_dCaAP(savepath):
	""" Function that normalizes the data and saves them in a matrix, or if such
		data already exist justs loads and returns the arrays. """
	
	# if not normalized data are loaded perform normalization
	if savepath in raw_data:
		# Load the saved variables
		data = np.load(savepath + ".npz")
		M = data["sols_matrix"]
		
		# find the minimum and maximum values in the data, considering only the 
		# parameters that actually compute each particular Boolean Function
		acc100 = M[:,-1,bf]==100
		M100 = M[:,0:4,bf][acc100]
		mx = np.max(M100[:,0:4])
		mn = np.min(M100[:,0:4])

		# normalize the data with the respect to the max value of the 4 variables
		# we are interested in (w11,w12,b1,b2)
		if mx > mn:
			mxx = mx
		else:
			mxx = mn
		
		M[:,0:4,:] = M[:,0:4,:] / mxx

		# also normalize the range within which the dCaAP outputs 1
		dCaAP_center_min = np.array([0.4725]) / mxx
		dCaAP_center_max = np.array([0.7]) / mxx
		
		# save the normalized data, so that the procedure doesn't have to be
		# repeated every time
		np.savez_compressed(savepath + "_norm",
							sols_matrix=M,
							dCaAP_center_min=dCaAP_center_min,
							dCaAP_center_max=dCaAP_center_max)
	
	# if normalized data are loaded just return the arrays
	elif savepath in norm_data:
		# Load the saved variables
		data = np.load(savepath + ".npz")
		M = data["sols_matrix"]
		dCaAP_center_min = data["dCaAP_center_min"]
		dCaAP_center_max = data["dCaAP_center_max"]
	
	return M, dCaAP_center_min, dCaAP_center_max
