from mpl_toolkits.mplot3d import Axes3D                                        # needed for 3D scatterplot

import os,sys
import numpy as np
import matplotlib.pyplot as plt

###############################################################################
# Create matrices to keep the data
IV = np.empty([9261,4,11])                              # initial values matrix
M = np.empty([9261,13,11])                              # final values matrix

# Initiate counts for correct data insertion in each array
b2_count = 0
matrix_count = 0

# Load the saved data
basepath = "/home/cluster/sotirisp/xor/final_data/2_nodes_dCaAP/reg"    # data path
dir = os.listdir(basepath)

for d in dir:
    # create full path
    new_basepath = basepath + "/" + d
    d = os.listdir(new_basepath)
    for f in d:
        filename = new_basepath + "/" + f
        data = np.load(filename)
    	
        init_values = data["init_values"]		# retrieve all initial...
        IV[matrix_count,0,b2_count] = init_values[0]	# ...weights and bias values...
        IV[matrix_count,1,b2_count] = init_values[1]	# ...and store them
        IV[matrix_count,2,b2_count] = init_values[3]    # W21 is not needed!!!
        IV[matrix_count,3,b2_count] = init_values[4]
        
        weights = data["weights"]		# retrieve all weights, bias...
        bias = data["bias"]			# ...and loss values...
        #av_loss = data["av_loss"]		# ...throughout epochs
    
        target_output = data["target_output"]	# retrieve target output and...
        output = data["output"]			# ...actual output in each epoch
    
        accuracy = data["accuracy"]		# retrieve accuracies with...
        #per_input_accs = data["per_input_accuracies"]	# ...cut-off set at 0.5

        M[matrix_count,0:2,b2_count] = weights[0:2].T   # put final parameters...
                                                        # ...values in matrix M

        M[matrix_count,2:4,b2_count] = bias.T

        #M[matrix_count,3:4,lo_count] = av_loss[-1].T

        M[matrix_count,4:8,b2_count] = target_output.T
        M[matrix_count,8:12,b2_count] = output.T                                                   

        M[matrix_count,-1,b2_count] = accuracy.T

        matrix_count += 1

    # move to the next logical operation and reset matrix_count
    b2_count += 1
    matrix_count = 0

###############################################################################
# Create larger matrices that contain all the simulations that have ended up in
# 100% accuracy

# initialize a new count for correct data insertion
count = 0

# take advantage of the b2_count, in order to avoid hard-coding how many there
# should be
for b2 in range(0,b2_count):
    # keep the 100% accuracy combinations of each b2 initialization
    acc100 = M[:,-1,b2]==100
    b2_in_sol = IV[:,0:4,b2][acc100]
    b2_f_sol = M[:,0:4,b2][acc100]
    if b2_f_sol.size != 0:
        count += 1
        # keep the average error of each solution
        error = abs( M[:,4:8,b2][acc100] - M[:,8:12,b2][acc100] )
        b2_error = np.average(error, axis=1)
        b2_error = b2_error.reshape(b2_error.shape[0],1)
        # merge the error with the corresponding solutions
        b2_sol_error = np.concatenate((b2_f_sol,b2_error),axis=1)
        # for the first solutions create a new matrix
        if count==1:
            IVsols = b2_in_sol
            Msols = b2_sol_error
        # for subsequent solutions append the solutions to the matrix
        else:
            IVsols = np.vstack((IVsols,b2_in_sol))
            Msols = np.vstack((Msols,b2_sol_error))

###############################################################################
# Perform PCA (or not) on M with 3 initial dimensions (weights and bias)
prepr_method = "PCA" #"None"

if not Msols.any():
    # if there exist no solutions then exit the code with a message; else there
    # is no need to check again
    sys.exit("!!! ---- ZERO DATA POINTS WITH 100% ACCURACY ---- !!!")
elif Msols.any() and prepr_method == "PCA":
    pca = PCA(n_components=3, svd_solver="full")                                # reduce to 3D space
    Mnew = pca.fit_transform(Msols)                                             # (comparable to ONNN)
elif Msols.any() and prepr_method == "None":
    Mnew = Msols

###############################################################################
# Normalize the data, before applying the DBSCAN clustering algorithm, with
# minmax_scale
Mscaled = minmax_scale(Mnew,feature_range=(0,1),axis=0)

# Use NearestNeighbors in order to determine the Eps (Ester et al. 1996)
nbrs = NearestNeighbors(n_neighbors=2*Mscaled.shape[1]-1,).fit(Mscaled)         # see Sander et al. (1998)
distances, indices = nbrs.kneighbors(Mscaled)
# Sort in descending order
distances = -np.sort(-distances,axis=0)
distances = np.sum(distances[:,1:],axis=1)
# Create a sorted k-distance plot: Eps value should be near the first "valley"
fig3 = plt.figure()
ax3 = fig3.add_subplot(111)
ax3.plot(distances)
plt.pause(15)                                                                   # you have only 15s!!!
plt.close(fig3)                                                                 # no need for more, it gets annoying!

# Perform DBSCAN clustering and retrieve the labels
while True:
    eps = input("Set the Eps value according to the k-distance plot: ")
    try:
        eps = float(eps)
        if isinstance(eps,float):                                               # radius ε
            break
        else:
            print("\nEps should be float!")
    except ValueError:
        print("\nEps should be float!")

min_pts = 2*Mnew.shape[1]                                                       # density in ε: Sander et al. (1998)

dbscan = DBSCAN(eps=eps,min_samples=min_pts)
labels = dbscan.fit_predict(Mscaled)                                            # name chosen for consistency
no_cl = len(set(labels)) - (1 if -1 in labels else 0)

# Separate the data points that are clustered as noise by the DBSCAN algorithm
noise = labels==-1
clustered = labels!=-1

n_labels = labels[noise]
cl_labels = labels[clustered]

# Make a check that not all data are considered noise!
if len(n_labels) == len(labels):
    print("!!! ---- ALL DATA POINTS CLUSTERED AS NOISE ---- !!!")
    # if all data points have been assigned to the noise label, consider much
    # larger eps values
    
###############################################################################
# Control specific matplotlib aspects for proper data plotting

# Define how the different b2 initializations will be mapped to cmap="Paired"
bc = np.linspace(np.min(IV[:,3,:]),np.max(IV[:,3,:]),num=b2_count)

# Define how the 100%-accuracy subplots' colors will be mapped to cmap="autumn_r"
a = Msols[:,-1]

# Define black as the color for noise (not grouped in clusters) for better
# visualization (the clustered labels will be automatically assigned to 
# cmap="viridis")
n_color = "k"

# Get the percentage of the combinations that resulted in 100% accuracy
per = len(a)/float(M.shape[0]*b2_count)*100

###############################################################################
# Plot the original and final data before applying the PCA
    
# Plot the original data
fig1 = plt.figure()
ax11 = fig1.add_subplot(121,projection='3d')
ax12 = fig1.add_subplot(122,projection='3d')

fig1titletext0 = "XOR (dCaAP)"
fig1titletext1 = "\nParameter Space of initial values"
fig1title = fig1titletext0 + fig1titletext1

fig1.suptitle(fig1title,fontsize=12,fontweight='bold',verticalalignment='top',
                style='oblique')

ax11.set_xlabel("Initial w11", fontsize=12)
ax11.set_ylabel("Initial w12", fontsize=12)
ax11.set_zlabel("Initial b1", fontsize=12)
ax11.set_title("All initial values combinations leading to solutions",fontsize=12)

ax12.set_xlabel("Initial w11", fontsize=12)
ax12.set_ylabel("Initial w12", fontsize=12)
ax12.set_zlabel("Initial b1", fontsize=12)
ax12titletext1 = "Initial combinations reaching 100% accuracy"
ax12titletext2 = "\n({0:.2f}% of total combinations)".format(per)
ax12title = ax12titletext1 + ax12titletext2
ax12.set_title(ax12title, fontsize=12)

p11 = ax11.scatter(IVsols[:,0],IVsols[:,1],IVsols[:,2],c=IVsols[:,3],cmap="tab10")
p12 = ax12.scatter(IVsols[:,0],IVsols[:,1],IVsols[:,2],c=a,cmap="autumn_r")

# Control the position and size of the bargraphs
c11ax = plt.axes([0.1,0.2,0.01,0.6])                                            # x,y,thickness,h
c12ax = plt.axes([0.92,0.2,0.01,0.6])                                           # x,y,thickness,h
cbar11 = plt.colorbar(p11,ticks=bc,cax=c11ax)
cbar12 = plt.colorbar(p12,cax=c12ax)
cbar11.set_label("Initial b2")
cbar12.set_label("Average distance from target output for 100% accuracy set (a.u.)")


# Plot the final results, before applying PCA (but use the info of the clustering)
fig2 = plt.figure()
ax21 = fig2.add_subplot(121,projection='3d')
ax22 = fig2.add_subplot(122,projection='3d')

fig2titletext0 = "XOR (dCaAP)"
fig2titletext1 = "\nParameter Space of final values"
fig2title = fig2titletext0 + fig2titletext1

fig2.suptitle(fig2title,fontsize=10,fontweight='bold',verticalalignment='top',
                style='oblique')

ax21.set_xlabel("Final w11", fontsize=10)
ax21.set_ylabel("Final w12", fontsize=10)
ax21.set_zlabel("Final b1", fontsize=10)
ax21.set_title("All solutions", fontsize=10)
        
ax22.set_xlabel("Final w11", fontsize=10)
ax22.set_ylabel("Final w12", fontsize=10)
ax22.set_zlabel("Final b1", fontsize=10)
ax22titletext1 = "Final combinations (w11,w12,b1) reaching 100% accuracy"
ax22titletext2 = "\n({0:.2f}% of total combinations)".format(per)
ax22title = ax22titletext1 + ax22titletext2
ax22.set_title(ax22title, fontsize=10)

p21 = ax21.scatter(Msols[:,0],Msols[:,1],Msols[:,2],c=Msols[:,3],cmap='viridis')
p22 = ax22.scatter(Msols[:,0],Msols[:,1],Msols[:,2],c=a,cmap="autumn_r")

# Control the position and size of the bargraphs
c21ax = plt.axes([0.1,0.2,0.01,0.6])                                            # x,y,thickness,h
c22ax = plt.axes([0.92,0.2,0.01,0.6])                                           # x,y,thickness,h
cbar21 = plt.colorbar(p21,cax=c21ax)    
cbar22 = plt.colorbar(p22,cax=c22ax)
cbar21.set_label("Final b2")
cbar22.set_label("Average distance from target output for 100% accuracy set (a.u.)")

###############################################################################
# Plot the transformed data after applying the PCA (or not) and grouping the 100%
# accuracy set with DBSCAN

# Create the appropriate title
fig3titletext0 = "XOR (dCaAP)"
fig3titletext1 = "\nParameter Space of transformed values with 100% accuracy, clustered"

if no_cl == 1:
    groups_str = "group"
elif no_cl > 1:
    groups_str = "groups"
elif no_cl == 0:
    groups_str = "no groups"
    
fig3titletext2 = "\nin {0} ".format(no_cl) + groups_str + " using DBSCAN"
boxstr = "eps = {0:.2f}\nMinPts = {1:.0f}\n".format(eps,min_pts)

fig3title = fig3titletext0 + fig3titletext1 + fig3titletext2

if any(noise):
    warning = "\n\n(black color indicates points that are classified as noise)"
    fig3title = fig3titletext0 + fig3titletext1 + fig3titletext2 + warning
else:
    fig3title = fig3titletext0 + fig3titletext1 + fig3titletext2

# Plot either the 3D PC space or the original (leaving out the b2 values)
if prepr_method == "PCA":
    fig3 = plt.figure()
    ax3 = fig3.add_subplot(111,projection='3d')    
    ax3.set_xlabel("PC 1: {0:.4f}%".format(pca.explained_variance_ratio_[0]),
                    fontsize=10)
    ax3.set_ylabel("PC 2: {0:.4f}%".format(pca.explained_variance_ratio_[1]),
                    fontsize=10)
    ax3.set_zlabel("PC 3: {0:.4f}%".format(pca.explained_variance_ratio_[2]),
                    fontsize=10)

    ax3.scatter(Mscaled[:,0][clustered],Mscaled[:,1][clustered],
                   Mscaled[:,2][clustered],c=cl_labels,cmap="Paired")
    ax3.text2D(0.01,0.5,boxstr,fontdict=None,withdash=False,
            transform=ax3.transAxes,fontsize=12)
    
    if any(noise):
        ax3.scatter(Mscaled[:,0][noise],Mscaled[:,1][noise],Mscaled[:,2][noise],
                    c=n_color) 

    ax3.set_title(fig3title,fontsize=10,fontweight='bold',
                    verticalalignment='bottom',style='oblique')

    plt.tight_layout()

elif prepr_method == "None":
    fig3 = plt.figure()
    ax3 = fig3.add_subplot(111,projection='3d')    
    ax3.set_xlabel("Final w11",fontsize=10)
    ax3.set_ylabel("Final w12",fontsize=10)
    ax3.set_zlabel("Final b1",fontsize=10)

    ax3.scatter(Mscaled[:,0][clustered],Mscaled[:,1][clustered],
                   Mscaled[:,2][clustered],c=cl_labels,cmap="Paired")
    ax3.text2D(0.01,0.5,boxstr,fontdict=None,withdash=False,
            transform=ax3.transAxes,fontsize=12)
    
    if any(noise):
        ax3.scatter(Mscaled[:,0][noise],Mscaled[:,1][noise],Mscaled[:,2][noise],
                    c=n_color) 

    ax3.set_title(fig3title,fontsize=10,fontweight='bold',
                    verticalalignment='bottom',style='oblique')

    plt.tight_layout()

plt.show()
