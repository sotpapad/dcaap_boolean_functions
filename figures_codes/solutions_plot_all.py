#! python3

# This is a script that receives as inputs sets of values (computed after
# clustering) and creates easy to understand plots of the combinations of the
# weights and bias that solve each Boolean Function with 1 node

from __future__ import generators, print_function
from six.moves import input

import matplotlib.pyplot as plt
import matplotlib.transforms as mtransforms
import numpy as np

################################################################################
# Definition of the dendritic transfer functions

# dCaAP
def dCaAP(x):
	""" Difference of exponentials that closely resembles the experimental
		data for the dendritic activation function. Outputs >0.5 in the range
		[0.4725, 0.70]. """
	func_array = np.empty([x.shape[0],1])
	for index, item in enumerate(x):
		if item < 0.469517:
			func_array[index] = 0.0
		else:
			func_array[index] = np.exp( (-item + 0.5)/0.3 ) - \
								np.exp(-210*(item-0.47))
	return func_array

# Steep Sigmoid
def sigmoid(x,steep,center):
	""" Sigmoid dendritic activation function. Outputs >0.5 for x>0.5. """
	return 1./( 1 + np.exp(steep*(-x+center)) )

# Narrow Gaussian
def gaussian(x):
	""" Gaussian activation function; used to validate dCaAP, because of their
		resemblance. Outputs >0.5 in the range [0.30, 0.70]."""
	return np.exp(-(x-0.5)**2/0.063)

################################################################################
# Creation of the lines for the plots

# Specification of input and generation of the curves of the dCaAP and Gaussian
# activation functions
x = np.linspace(0,1.5,num=15000)
y1 = dCaAP(x)
y2 = sigmoid(x,25,0.6).reshape(-1,1)
y3 = gaussian(x)

################################################################################
# Specification of the name of the Boolean function whose solutions are
# being plotted each time
bf = input("Which Boolean Function are these solutions for? ")
bf = bf.upper()

################################################################################
# Specification of the number of subplots to be created, given the conceptual
# solutions that arise after the clustering of the data

while True:
	num_sols = input("How many solutions exist for this Boolean Function? ")
	try:
		num_sols = int(num_sols)
		
		if num_sols == 0:
			print("This code doesn't work for no solutions!")
		elif isinstance(num_sols,int):
			break
		else:
			print("Number of solutions should be an integer value!")
	except ValueError:
		print("Please respond with an integer!")
		
################################################################################
# Given a number of solutions create a dictionary that holds the combinations
# of values for each specific solution

# Create a dictionary that stores all dictionaries
sols = {}

# Ask for the values of each solution separately and store them all
prms = ["b1","w11","w12"]
for n in range(num_sols):
	# Create an empty dictionary that will hold the values of each solution
	comb = {}
	print("\nNow defining the values of the parameters for solution " + str(n+1)\
		  +".")
	for prm in range(len(prms)):
		while True:
			ans = input("What is the value of " + prms[prm] + "? ")
			try:
				ans = float(ans)
				if isinstance(ans,float):
					comb[prms[prm]] = ans
					break
				else:
					print("Have you entered a number as input?")
					#break
			except ValueError:
				print("Have you entered a number as input?")
	sols[str(n)]=comb

################################################################################
# Definition of common parameters for all plots

# Specify fontsize
fs = 10

# Specify axes limits
x_right = 1.6
x_left = -0.5
y_top = 1.7
y_bottom = 0.0

# Specify the y ticks positions
y_ticks = [0,0.5,1.0]

# Specify the color for the activation function, the background where input is
# considered to surpass the threshold and the bias
af_color = (0.05,0.05,0.05)	# (dark) gray
bg_color = (0.9,0.8,1.0)	# gray-purple
b_color = (0.8,0.8,0.8)		# (light) grey

# Create a list of colors. Each color will be used to indicate a different
# solution (purple,blue,black)
colors = [(0.7,0.3,0.8), (0.0,0.5,1.0), (0.0,0.0,0.0)]

################################################################################
# Initialize two plots

# The first plot, comparatively displays solutions for the three activation
# functions (note that they should all be put as input together)
# The plot should always have two rows, one for each activation function
fig1 = plt.figure()
fig1.suptitle(bf,fontweight='bold',verticalalignment='top')

ax11 = fig1.add_subplot(311)
ax12 = fig1.add_subplot(312)
ax13 = fig1.add_subplot(313)

# Get rid of the right and top spines
ax11.spines['top'].set_visible(False)
ax11.spines['right'].set_visible(False)
ax12.spines['top'].set_visible(False)
ax12.spines['right'].set_visible(False)
ax13.spines['top'].set_visible(False)
ax13.spines['right'].set_visible(False)

# Set the x and y-axis limits
ax11.set_xlim(x_left,x_right)
ax12.set_xlim(x_left,x_right)
ax13.set_xlim(x_left,x_right)
ax11.set_ylim(y_bottom,y_top)
ax12.set_ylim(y_bottom,y_top)
ax13.set_ylim(y_bottom,y_top)

# Choose the yticks
ax11.set_yticks(y_ticks)
ax12.set_yticks(y_ticks)
ax13.set_yticks(y_ticks)

# Plot the dCaAP in the upper subplot, the Sigmoid in the middle and the
# Gaussian in the lower
ax11.plot(x,y1,color=b_color,linewidth=2)
ax12.plot(x,y2,color=b_color,linewidth=2)
ax13.plot(x,y3,color=b_color,linewidth=2)


# The second plot only displays the solutions for the dCaAP
fig2 = plt.figure()
fig2.suptitle(bf,fontweight='bold',verticalalignment='top')
ax2 = fig2.add_subplot(111)
# Get rid of the right and top spines
ax2.spines['top'].set_visible(False)
ax2.spines['right'].set_visible(False)
# Set the x and y-axis limits
ax2.set_xlim(x_left,x_right)
ax2.set_ylim(y_bottom,y_top)		# change it here, because with 1 subplot less is enough
# Choose the yticks and y label
ax2.set_ylabel("Output",fontsize=fs)
ax2.set_yticks(y_ticks)
# Plot the dCaAP(y1) or the sigmoid(y2)
ax2.plot(x,y1,color=b_color,linewidth=3)

################################################################################
# Iteratively plot the results

# Create an empty dictionary that will hold the sums of the prms in sols in
# order to be able to dynamically set the xticks
sums = {}
count = 1
for sol in sols:
	sm = {}
	sm['b1'] = sols[sol]['b1']
	sm['b1w11'] = sols[sol]['b1']+sols[sol]['w11']
	sm['b1w12'] = sols[sol]['b1']+sols[sol]['w12']
	sm['b1w11w12'] = sols[sol]['b1']+sols[sol]['w11']+sols[sol]['w12']
	sums[str(sol)]=sm

	# Set the x-coordinates of the prms in each comb in sols
	b1 = sums[str(sol)]['b1']
	b1w11 = sums[str(sol)]['b1w11']
	b1w12 = sums[str(sol)]['b1w12']
	b1w11w12 = sums[str(sol)]['b1w11w12']

	# Choose the xticks
	if abs(0.5-round(b1,2)) < 0.02 or abs(0.6-round(b1,2)) < 0.02:
		xticks = [0,1.0,round(b1,2),round(b1w11,2),round(b1w12,2),
				round(b1w11w12,2)]
	else:
		xticks = [0,0.5,0.6,1.0,round(b1,2),round(b1w11,2),round(b1w12,2),
				round(b1w11w12,2)]

	# Create labels for the vertical lines that represent each comb for each
	# solution
	bias = "$b_{1}$"
	weight1 = "$w_{11}$"
	weight2 = "$w_{12}$"
	ex1 = "$X_{1}$"
	ex2 = "$X_{2}$"
	vlines_labels = [bias, bias+"+"+ex1+weight1, bias+"+"+ex2+weight2,
					bias+"+"+ex1+weight1+"+"+ex2+weight2]

	# only plot vertical lines that indicate where the bias (initially) is for
	# each solution
	l1 = ax11.axvline(x=b1,c=b_color,lw=2,ls="--",alpha=0.8)					# plot b1
	l1 = ax12.axvline(x=b1,c=b_color,lw=2,ls="--",alpha=0.8)					# plot b1
	l1 = ax13.axvline(x=b1,c=b_color,lw=2,ls="--",alpha=0.8)					# plot b1
	l2 = ax2.axvline(x=b1,c=b_color,lw=2,ls="--",alpha=0.8)						# plot b1

	# initialize 2 lists to store all the arrows that will be created, so that
	# figure legends works properly
	arrows1 = []
	arrows2 = []

	# plot arrows to better visualize how the Xiw1i shifts the b1
	kw = dict(length_includes_head=True,head_width=0.03,head_length=0.01,
			  alpha=0.8)
	# b1+X1w11
	if b1 != b1w11:
		# arrow<combination # (1,2,3)>_<subplot-subplot #>
		arrow1_11 = ax11.arrow(b1,1.05+(count-1)*0.15,b1w11-b1,0,
							color=colors[0],**kw)
		arrows1.append(arrow1_11)
		arrow1_12 = ax12.arrow(b1,1.05+(count-1)*0.15,b1w11-b1,0,
							color=colors[0],**kw)
		arrow1_13 = ax13.arrow(b1,1.05+(count-1)*0.15,b1w11-b1,0,
							color=colors[0],**kw)
		arrow1_2 = ax2.arrow(b1,1.05+(count-1)*0.15,b1w11-b1,0,
							color=colors[0],**kw)
		arrows2.append(arrow1_2)
	# b1 + X2w12
	if b1 != b1w12:
		arrow2_11 = ax11.arrow(b1,1.08+(count-1)*0.15,b1w12-b1,0,
							color=colors[1],**kw)
		arrows1.append(arrow2_11)
		arrow2_12 = ax12.arrow(b1,1.08+(count-1)*0.15,b1w12-b1,0,
							color=colors[1],**kw)
		arrow2_13 = ax13.arrow(b1,1.08+(count-1)*0.15,b1w12-b1,0,
							color=colors[1],**kw)
		arrow2_2 = ax2.arrow(b1,1.08+(count-1)*0.15,b1w12-b1,0,
							color=colors[1],**kw)
		arrows2.append(arrow2_2)
	# b1 + X1w11 + X2w12
	if b1 != b1w11w12:
		arrow3_11 = ax11.arrow(b1,1.11+(count-1)*0.15,b1w11w12-b1,0,
							color=colors[2],**kw)
		arrows1.append(arrow3_11)
		arrow3_12 = ax12.arrow(b1,1.11+(count-1)*0.15,b1w11w12-b1,0,
							color=colors[2],**kw)
		arrow3_13 = ax13.arrow(b1,1.11+(count-1)*0.15,b1w11w12-b1,0,
							color=colors[2],**kw)
		arrow3_2 = ax2.arrow(b1,1.11+(count-1)*0.15,b1w11w12-b1,0,
							color=colors[2],**kw)
		arrows2.append(arrow3_2)

	# fill in the area where each activation function is producing an output
	# of 1
	trans1 = mtransforms.blended_transform_factory(ax11.transData, ax11.transAxes)
	trans2 = mtransforms.blended_transform_factory(ax12.transData, ax12.transAxes)
	trans3 = mtransforms.blended_transform_factory(ax13.transData, ax13.transAxes)
	trans4 = mtransforms.blended_transform_factory(ax2.transData, ax2.transAxes)

	# create shaded area that indicates the range of actual solutions for dCaAP
	ax11.fill_between(x=np.arange(0.47,0.71,0.01),y1=0.0,y2=1.0,
					facecolor=bg_color, alpha=0.1, transform=trans1)
	
	# Add xticks (according to each sm in sums) in upper row
	# (according to each sm in sums)
	ax11.set_xlabel("Input")
	ax11.set_xticks(xticks)
	ax11.set_xticklabels(xticks,fontsize=9)

	# Create shaded area that indicates the range of actual solutions for Sigmoid
	ax12.fill_between(x=np.arange(0.6,1.4,0.01),y1=0.0,y2=1.0,
					facecolor=bg_color, alpha=0.1, transform=trans2)
	
	# Add xlabel and xticks (according to each sm in sums) in bottom row
	ax12.set_xlabel("Input")
	ax12.set_xticks(xticks)
	ax12.set_xticklabels(xticks,fontsize=9)
	
	# Create shaded area that indicates the range of actual solutions for Gaussian
	ax13.fill_between(x=np.arange(0.3,0.7,0.01),y1=0.0,y2=1.0,
					facecolor=bg_color, alpha=0.1, transform=trans3)
	
	# Add xlabel and xticks (according to each sm in sums) in bottom row
	ax13.set_xlabel("Input")
	ax13.set_xticks(xticks)
	ax13.set_xticklabels(xticks,fontsize=9)
	 
	count += 1

	# create shaded area that indicates the range of actual solutions for dCaAP,
	# for the second plot
	ax2.fill_between(x=np.arange(0.47,0.71,0.01),y1=0.0,y2=1.0,
					facecolor=bg_color, alpha=0.1, transform=trans4)
	
	# Add xticks (according to each sm in sums) in upper row
	# (according to each sm in sums)
	ax2.set_xlabel("Input",fontsize=fs)
	ax2.set_xticks(xticks)
	ax2.set_xticklabels(xticks,fontsize=fs)

# Create legends that shows how the background is changed by the inputs
fig1.legend(arrows1,vlines_labels[1:len(arrows1)+1],loc="upper right",
		   frameon=False,fontsize="small")
fig2.legend(arrows2,vlines_labels[1:len(arrows2)+1],loc="upper right",
		   frameon=False,fontsize=fs)

#plt.tight_layout()

plt.show()
