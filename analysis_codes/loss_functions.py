# necessary standard library imports
import numpy as np

# Definition of 2 potential loss functions to be used and their derivatives

# Logarithmic loss function and derivative
def log_loss(y,a2):
    """ Logistic cost function. """
    func_array = np.empty([a2.shape[0],1])
    
    for index, item in enumerate(a2):                                           # ensure numerical stability
        if item > 1.0:                                                          # sanity check: if dCaAP > 1 => 1
            func_array[index] == 1.0

        if (1-item) >= 0.999:
            func_array[index] = 0.1
        elif (1-item) <= 0.001:
            func_array[index] = 0.9
        else:
            func_array[index] = item
    
    return -y*np.log(func_array) - (1-y)*np.log(1-func_array)


def log_loss_der(y,a2):
    """ Derivative of logistic cost function. """
    loss_array = np.empty([y.shape[0],1])
    for index, item in enumerate(a2):                                           # ENSURE NUMERICAL STABILITY:
        if item > 1.0:                                                          # sanity check: if dCaAP > 1 => 1
            loss_array[index] == 1.0
                                           
        if item == 0.0:                                                         # control cases of one node...
            loss_array[index] = 0.1                                             # ...that can take on "0.0"
        elif item > 0.0 and (1-item) <= 0.1:                                    # difference between 1 and a2 is preferred...
            loss_array[index] = 0.9                                             # ...over the actual a2 value evaluation ( :P )...
        elif item > 0.0 and (1-item) >= 0.9:                                    # ...because a2 = 1.0/0.0 is usually an...
																				# ...approximation...
            loss_array[index] = 0.1                                             # ...and thus the α2 == 1.0/0.0 control...
        else:                                                                   # ...evaluates to FALSE => no a2 value reassignment
            loss_array[index] = item
    
    return -y/loss_array + (1-y)/(1-loss_array)


# Euclidean distance loss function
def mse_loss(y,a2):
    """ Mean squared error cost function. """
    return (y-a2)**2

def mse_loss_der(y,a2):
    """ Derivative of mean squared error cost function. """
    return 2*(a2-y)
