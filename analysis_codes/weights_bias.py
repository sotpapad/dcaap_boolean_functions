# Definition of the restrictions for the weights of the input layer to be explored 
# during the training process
def update_weights(weights, weights_choice):
	""" Impose restrictions to the weights update rule according to the weights
		initialization choice. The options for the weights initialization are:
		(1)Both permanently excitatory.
		(2)Both permanently inhibitory.
		(3)One permanently excitatory and one permanently inhibitory.
		(4)Initially both excitatory or inhibitory, but unconstrained. """
	
	for index, w in enumerate(weights[0:-1]):
		if weights_choice == 1 and w < 0.0:
			weights[index] = 0.001
		elif weights_choice == 2 and w > 0.0:
			weights[index] = -0.001
		elif weights_choice == 3:
			if weights[0] < 0.0:
				weights[0] = 0.001
			if weights[1] > 0:
				weights[1] = -0.001
		elif weights_choice == 4:
			pass

	return weights
	

# Definition of restrictions on biases' values updates
def update_bias(bias, bias_choice):
	""" Control bias update durnig main execution depending on the choice
		provided in restr_bias(). The options for biases update rule are: 
		(1)Unrestricted bias/biases.
		(2)B[0] restricted to positive values.
		(3)B[0] restricted to negative values.
		(4)Both biases restricted to positive values.
		(5)Both biases restricted to negative values.
		Note that in the case of 1 node simulation only one bias exists, thus
		making choices (2,4) and (3,5) practically the same! """
	
	if bias_choice == 1:
		pass
	elif bias_choice == 2 and bias[0] < 0.0:
		bias[0] = 0.001
	elif bias_choice == 3 and bias[0] > 0.0:
		bias[0] = -0.001
	elif bias_choice == 4:
		for index, b in enumerate(bias):                                        # works better than normal...
			if b < 0.0:                                                     # ...for loop; not sure why!
				bias[index] = 0.001
	elif bias_choice == 5:
		for index, b in enumerate(bias):
			if b > 0.0:
			   bias[index] = -0.001 

	return bias
