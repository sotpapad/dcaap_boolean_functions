# necessary standard library imports
import numpy as np

# Definition of L2 regularization for weights and loss

def regul(regul_choice,lamda,weights):
	if regul_choice == 1.0:
		return lamda*np.sum(w**2 for w in weights)
	elif regul_choice == 0.0:
		return 0.0
   
def regul_der(regul_choice,lamda,weights,target_output):
	if regul_choice == 1.0:
		return np.multiply(lamda/len(target_output),weights)
	elif regul_choice == 0.0:
		return 0.0
