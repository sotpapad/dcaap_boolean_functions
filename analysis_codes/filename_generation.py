# Definition of function that creates a unique name for each initialization with
# characteristics that allow different choices to be easily identified

# This function also makes sanity checks; if any of the variables is given a
# value outside its appropriate range, it will raise a system error and terminate
# the code execution

def filename_generation(weights_choice,bias_choice,activation_function,regul_choice,
						method_choice,input_pres_choice):
	""" Generation of descriptive, unique filename """

	# weights (see weights_bias.py)
	weights_choice = float(weights_choice)
	if weights_choice == 1.0:
		# force only positive weights
		w_str = "Pos_weights"
	elif weights_choice == 2.0:
		# force only negative weights
		w_str = "Neg_weights"
	elif weights_choice == 3.0:
		# note that what matters is that the two input pathways have opposite
		# values, not which is which
		w_str = "Pos-Neg_weights"
	elif weights_choice == 4.0:
		# do not impose restrictions on weights
		w_str = "Unr_weights"                                   										
	else:
		raise SystemExit("\nAborting code execution because of bad " +
							"weights initialization!")

	# bias (see weights_bias.py)
	bias_choice = float(bias_choice)
	if bias_choice == 1.0:
		b_str = "Unr_bias"
	elif bias_choice == 2.0:
		b_str = "B[0]pos_bias"
	elif bias_choice == 3.0:
		b_str = "B[0]neg_bias"
	elif bias_choice == 4.0:
		b_str = "Pos_bias"
	elif bias_choice == 5.0:
		b_str = "Neg_bias"
	else:
		raise SystemExit("\nAborting code execution because of bad "
							"biases initialization!")

	# activation function (see activation_functions.py)
	activation_function = float(activation_function)
	if activation_function == 1.0:
		act_func_str = "dCaAP"
	elif activation_function == 2.0:
		act_func_str = "sigmoid"    
	elif activation_function == 3.0:
		act_func_str = "gaussian"
	else:
		raise SystemExit("\nAborting code execution because of bad "
							"activation function initialization!")
	
	# regularization (see regularization.py)
	regul_choice = float(regul_choice)
	if regul_choice == 0.0:
		reg_str = "noreg"
	elif regul_choice == 1.0:
		reg_str = "reg"
	else:
		raise SystemExit("\nAborting code execution because of bad "
							"regularization initialization!")
							
	# input presentation choice
	input_pres_choice = float(input_pres_choice)
	if input_pres_choice == 1.0:
		input_str = "Std_batch"
	elif input_pres_choice == 2.0:
		input_str = "Shfl_batch"
	elif input_pres_choice == 3.0:
		input_str = "Rand_batch"
	else:
		raise SystemExit("\nAborting code execution because of bad "
							"input_pres_method initialization!")
	
	# run code as vectorized or loop (see run_method.py)
	method_choice = float(method_choice)
	if method_choice == 1.0 and input_pres_choice == 3.0:
		str = ("No point in averaging given completely random "
				"inputs! You have to choose the per-input update "
				"rule (2)!\n")
		raise SystemExit(str)
	elif method_choice == 1.0:
		method_str = "Vectorized"
	elif method_choice == 2.0:
		method_str = "Loop"
	else:
		raise SystemExit("\nAborting code execution because of bad "
							"update method initialization!")

	
	return w_str, b_str, act_func_str, reg_str, method_str, input_str

