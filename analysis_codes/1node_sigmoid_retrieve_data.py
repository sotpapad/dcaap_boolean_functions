#! python3

from __future__ import generators, print_function
from six.moves import input

import os
import numpy as np

################################################################################
# Creation of dictionary that contains the names and corresponding paths of all
# 12 Boolean Functions
bf_paths = {"XOR": "/home/cluster/sotirisp/all_bfs/data/1node_sigmoid_noreg/xor",
			"XNOR": "/home/cluster/sotirisp/all_bfs/data/1node_sigmoid_noreg/xnor",
			"AND": "/home/cluster/sotirisp/all_bfs/data/1node_sigmoid_noreg/and",
			"NAND": "/home/cluster/sotirisp/all_bfs/data/1node_sigmoid_noreg/nand",
			"OR": "/home/cluster/sotirisp/all_bfs/data/1node_sigmoid_noreg/or",
			"NOR": "/home/cluster/sotirisp/all_bfs/data/1node_sigmoid_noreg/nor",
			"TRUE": "/home/cluster/sotirisp/all_bfs/data/1node_sigmoid_noreg/true",
			"FALSE": "/home/cluster/sotirisp/all_bfs/data/1node_sigmoid_noreg/false",
			"f4(0100)": "/home/cluster/sotirisp/all_bfs/data/1node_sigmoid_noreg/f4",
			"f5(0101)": "/home/cluster/sotirisp/all_bfs/data/1node_sigmoid_noreg/f5",
			"f12(1100)": "/home/cluster/sotirisp/all_bfs/data/1node_sigmoid_noreg/f12",
			"f13(1101)": "/home/cluster/sotirisp/all_bfs/data/1node_sigmoid_noreg/f13"}

################################################################################
# Create 3D matrix to keep the data
epochs = 9999
initializations = 9261
# final values matrix
M = np.empty([epochs*initializations,4,12])

# Initiate counts for correct data insertion in each array
bf_count = 0
matrix_count = 0

# Create a list that stores the order of logical operations
order = []

# Load the saved data (1 logical operation per time)
for bf_name, bf_path in bf_paths.items():
	# data path
	basepath = bf_path
	directory = os.listdir(basepath)

	for file in directory:
		# create full path
		filename = basepath + "/" + file
		data = np.load(filename)

		# retrieve all weights, bias, accuracy throughout epochs
		weights = data["weights"]
		bias = data["bias"]
		accuracy = data["accuracy"]

		# insert the values into the matrix
		M[epochs*matrix_count:epochs*(matrix_count+1),0:2,bf_count] = weights[:]
		M[epochs*matrix_count:epochs*(matrix_count+1),2:3,bf_count] = bias[:]
		M[epochs*matrix_count:epochs*(matrix_count+1),3:4,bf_count] = accuracy[:]

		matrix_count += 1

	# append the name of the logical operation parsed in each loop
	order.append(bf_name)
	# move to the next logical operation and reset matrix_count
	bf_count += 1
	matrix_count = 0
	
################################################################################
# Save the results of the above process, so that it is not repeated each time
save_path = "/home/cluster/sotirisp/all_bfs/data/all_sigmoid"
np.savez_compressed(save_path, bf_order=order, sols_matrix=M)
