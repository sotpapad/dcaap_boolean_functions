# necessary local imports
import network
import exitmain
import input_pres
import accuracy_estimation
from create_plots import create_plots

# Definition of two possible methods of running the code

# Vectorized (Suggested and used)
def vectorized(X, Y, Yname, W, B, lrate, slope, centers, max_epochs, epochs_el,
				lamda, acc, allA2, allY, av_L, w_val, b_val, input_pres_choice,
				nodes_choice, weights_choice, bias_choice, dendritic_function,
				method_choice, regul_choice, early_stop):
	""" Run the main percetron code in vectorized form. """

	# The first weights and bias values correspond to those before the iteration
	# begins
	w_val[0] = W.T
	b_val[0] = B.T

	for k in range(max_epochs):
		# renew the elapsed epochs value
		epochs_el = k
		
		# choose input format and present input to perceptron
		Xnew, Ynew = input_pres.call_batch(X, Y[Yname], input_pres_choice)
		
		# check the number of nodes and propagate (f-b-up)
		if nodes_choice == 1.0:
			nd = network.Perceptron(weights_choice, bias_choice, dendritic_function,
									regul_choice, "log_loss")
			A2, W, B, av_l = nd.prop(Xnew, Ynew, W, B, lrate, slope, lamda, centers,
										weights_choice, bias_choice, dendritic_function,
										regul_choice, method_choice)
		elif nodes_choice == 2.0:
			nd = network.MLPerceptron(weights_choice, bias_choice, dendritic_function,
										regul_choice, "log_loss")
			A2, W, B, av_l = nd.prop(Xnew, Ynew, W, B, lrate, slope, lamda, centers,
										weights_choice, bias_choice, dendritic_function,
										regul_choice, method_choice)
		
		allY[k] = Ynew.T
		allA2[k] = A2.T
		av_L[k] = av_l							# store average loss
		
                if not k+1 >= max_epochs:
			# the renewed values correspond to the next epoch
			w_val[k+1] = W.T					# store weights values
			b_val[k+1] = B.T					# store biases values
		
		#~ # compute accuracy in each epoch
		#~ acc[k] = accuracy_estimation.countacc(Xnew, Yname, A2)
		
		#~ if acc[k] == 100:
			#~ if b_val[k] >= 0.4725 and b_val[k] <= 0.7:
		#~ #if k == 4000:
				#~ print("wrong epoch: ", k)
				#~ print("accuracy: ", acc[k])
				#~ print("input: ", Xnew)
				#~ print("Y: ", Ynew)
				#~ print("output: ", A2)
				#~ print("bias: ", b_val[k-1:k+2])
				#~ print("weights: ", w_val[k-1:k+2])
		
		# control early code termination
		if early_stop == 1.0:
			# if early termination is allowed, evaluate conditions
			converged_output = exitmain.exitmain(av_L, k)
			# if conditions are met, stop
			if converged_output == True:
				break

	create_plots(Xnew, max_epochs, av_L, acc, W, B, w_val, b_val)

	return epochs_el

# Loop (mainly for validation purposes)
def loop(X, Y, Yname, W, B, lrate, slope, centers, max_epochs, epochs_el,
			lamda, acc, allA2, allY, av_L, w_val, b_val, input_pres_choice,
			nodes_choice, weights_choice, bias_choice, dendritic_function,
			method_choice, regul_choice, early_stop):

	# The first weights and bias values correspond to those before the iteration
	# begins
	w_val[0] = W.T
	b_val[0] = B.T

	""" Run the main percetron code in one-input-per-epoch form. """
	for k in range(max_epochs):
		# renew the elapsed epochs value
		epochs_el = k
		
		# choose input format and  present input to perceptron
		Xnew, Ynew = input_pres.call_batch(X, Y[Yname], input_pres_choice)

		for index, x in enumerate(Xnew):
			# change lrate in order for algorithm to converge properly in "for"
			#lrate = 0.0001
			
			# check the number of nodes and propagate (f-b-up)
			if nodes_choice == 1.0:
				nd = network.Perceptron(weights_choice, bias_choice, dendritic_function,
											regul_choice, "log_loss")
				A2, W, B, av_l = nd.prop(Xnew[index], Ynew[index], W, B, lrate,
										slope, lamda, centers, method_choice)

			elif nodes_choice == 2.0:
				nd = network.MLPerceptron(weights_choice, bias_choice, dendritic_function,
											regul_choice, "log_loss")
				A2, W, B, av_l = nd.prop(Xnew[index], Ynew[index], W, B, lrate,
										slope, lamda, centers, method_choice)
			
			allY[k][index] = Ynew
			allA2[k][index] = A2
			av_L[k] = av_l						# store average loss

			if not k+1 >= max_epochs:
				# the renewed values correspond to the next epoch
				w_val[k+1] = W.T				# store weights values
				b_val[k+1] = B.T				# store biases values
			
		# compute accuracy in each epoch
		acc[k] = accuracy_estimation.countacc(Xnew, Yname, allA2[k])
		
		# control early code termination
		if early_stop == 1.0:
			# if early termination is allowed, evaluate conditions
			converged_output = exitmain.exitmain(av_L, k)
			# if conditions are met, stop
			if converged_output == True:
				break

	return epochs_el
