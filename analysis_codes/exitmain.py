# necessary standard library imports
import numpy as np

# Definition of function that allows the code to terminate before reaching the
# maximum number of epochs, if the accuracy and the loss have converged

def exitmain(av_loss, k):
	""" Control early code termination. Two conditions must be met:
		the average loss has to stop changing significantly and the
		output values must sufficiently approximate the true ones. """

	converged_output = False
	
	# if no more than 100 epochs have elapsed the code doesn't need to be executed
	# also, make sure that loss doesn't fluctuate a lot and ensure minimum loss
	# change in the last 100 epochs
	if k > 100 and \
		np.all(av_loss[k:k-100:-1] < 0.2) and \
		abs(av_loss[k] - av_loss[k-100]) <= 0.001:
			converged_output = True

	return converged_output
