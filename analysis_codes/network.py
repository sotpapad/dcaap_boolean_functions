# necessary standard library imports
import numpy as np

# necessary local imports
import activation_functions
import regularization
import weights_bias
import loss_functions

# Definition of the one-node (perceptron) and the two-node (multi-layered
# perceptron) analogs

# Each layer in this network comprises of just one node

# We think of the dendritic tuft as a node where two inputs converge and, along
# with the state at the dendrite (first bias), their integration takes place

# This can be seen isolated, or can be part of a two-step process, where the
# dendritic integration results in an output that is fed to the soma
# (second node/layer); there, a new integration takes place, considering also the
# state at the soma (second bias)

# The syntax that is used is selected so that it kind of becomes python-version
# agnostic; it makes class in 2.x work more like in 3.x, and works just fine
# for python 3.x

class MLPerceptron(object):
	""" This perceptron will integrate two inputs and through the dCaAP will    
	 compute any Boolean function, using only one node in the hidden (dendritic)
	 layer. """                                                                              

	# set up the necessary parameters of the perceptron
	def __init__(self, weights_choice, bias_choice, activation_function,
					regularization_choice, loss="log_loss"):
		""" Initialize necessary parameters for percetron to work properly. """
		if activation_function == 1.0:
			self.dendrite = activation_functions.dCaAP
			self.dendrite_der = activation_functions.dCaAP_der
		elif activation_function == 2.0:
			self.dendrite = activation_functions.sigmoid
			self.dendrite_der = activation_functions.sigmoid_der
		elif activation_function == 3.0:
			self.dendrite = activation_functions.gaussian
			self.dendrite_der = activation_functions.gaussian_der
		
		self.soma = activation_functions.sigmoid
		self.soma_der = activation_functions.sigmoid_der
		
		self.update_weights = weights_bias.update_weights
		self.update_bias = weights_bias.update_bias
		
		self.regularization = regularization.regul
		self.regularization_der = regularization.regul_der

		if loss == "log_loss":
			self.loss_function = loss_functions.log_loss
			self.loss_der = loss_functions.log_loss_der
		elif loss == "mse_loss":
			self.loss_function = loss_functions.mse_loss
			self.loss_der = loss_functions.mse_loss_der
	 
	# definition of the propagation algorithm
	def prop(self, input_data, target_output, weights, bias, lrate, slope, lamda,
			 centers, weights_choice, bias_choice, activation_function,
			 regularization_choice, method_choice):
		""" A concise hand-written learning algorithm based on backpropagation
			of gradient values. """
		# Forward propagation:
		Z1 = bias[0] + np.matmul(input_data,weights[0:-1])                      # e.g. X[0][0]*W[0] + X[0][1]*W[1]
		
		if activation_function != 2:
			A1 = self.dendrite(Z1)
		elif activation_function == 2:
			A1 = self.dendrite(Z1,slope,centers[1])
		
		Z2 = bias[-1] + np.multiply(A1,weights[-1])
		
		A2 = self.soma(Z2,slope,centers[0])

		# Backpropagation:
		in_l = self.loss_function(target_output,A2)                             # store individual losses in current epoch
		av_l = np.average(in_l).reshape(1,1) + \
				self.regularization(regularization_choice,lamda,weights)        # store average loss in current epoch
		
		dA2 = self.loss_der(target_output,A2)                  
		
		dZ2 = np.multiply(dA2,self.soma_der(Z2,slope,centers[0])) 
		
		dA1 = np.multiply(dZ2,weights[-1])
		
		if activation_function != 2:
			dZ1 = np.multiply(dA1,self.dendrite_der(Z1))
		elif activation_function == 2:
			dZ1 = np.multiply(dA1,self.dendrite_der(Z1,slope,centers[1]))
		
		dB = np.array([np.average(dZ1),np.average(dZ2)]).reshape(-1,1)          # divide bias by # of inputs
		
		if method_choice == 1:
			dW = np.matmul(input_data.T,dZ1)/len(input_data) + \
				self.regularization_der(regularization_choice,lamda,weights[0:-1],target_output)# last weight is fixed!
		elif method_choice == 2:
			dW = np.matmul((input_data.T).reshape(-1,1),dZ1)/len(input_data) + \
				self.regularization_der(regularization_choice,lamda,weights[0:-1],target_output)
		
		# Update rule:
		weights[0:-1] -= np.multiply(dW,lrate)
		weights = self.update_weights(weights,weights_choice)			# weights values restriction
		
		bias -= np.multiply(dB,lrate)
		bias = self.update_bias(bias,bias_choice)			        # biases values restriction
	   
		W = weights                                                             # assign values of weights and biases to...
		B = bias                                                                # ...matrices W and B for proper main()...
											# ...call after 1st loop is executed
		return A2, W, B, av_l


class Perceptron(MLPerceptron):
	""" Make an one-node(dendritic) unit analogous to the perceptron, to test
		the dendritic activation function's behaviour without the somatic
		integration. """
		
	def __init__(self, weights_choice, bias_choice, activation_function,
					regularization_choice, loss="log_loss"):
		""" Initialize attributes of parent class. """
		# python 2.x syntax that works for 3.x too
		super(Perceptron, self).__init__(weights_choice, bias_choice,
								activation_function, regularization_choice,
								loss="log_loss")
	
	def prop(self, input_data, target_output, weights, bias, lrate, slope, lamda,
			 centers, weights_choice, bias_choice, activation_function,
			 regularization_choice, method_choice):
		""" A concise hand-written learning algorithm based on backpropagation
			of gradient values. """
		# Forward propagation:
		Z1 = bias[0] + np.matmul(input_data,weights)                            # e.g. W[0]*X[0][0] + W[1]*X[0][1]

		if activation_function != 2:
			A1 = self.dendrite(Z1)
		elif activation_function == 2:
			A1 = self.dendrite(Z1,slope,centers[1])
		A2 = A1                                                                 # for consistency purposes

		# Backpropagation:
		in_l = self.loss_function(target_output,A1)                             # store individual losses in current epoch
		av_l = np.average(in_l).reshape(1,1) + \
				self.regularization(regularization_choice,lamda,weights)        # store average loss in current epoch
		
		dA1 = self.loss_der(target_output,A1)                  
		
		if activation_function != 2:
			dZ1 = np.multiply(dA1,self.dendrite_der(Z1))
		elif activation_function == 2:
			dZ1 = np.multiply(dA1,self.dendrite_der(Z1,slope,centers[1]))

		dB = np.array([np.average(dZ1)]).reshape(-1,1)                          # divide bias by # of inputs

		if method_choice == 1:
			dW = np.matmul(input_data.T,dZ1)/len(input_data) + \
				self.regularization_der(regularization_choice,lamda,weights,target_output)      # last weight is fixed!
		elif method_choice == 2:
			dW = np.matmul((input_data.T).reshape(-1,1),dZ1)/len(input_data) + \
				self.regularization_der(regularization_choice,lamda,weights,target_output)
		
		# Update rule:
		weights -= np.multiply(dW,lrate)
		weights = self.update_weights(weights,weights_choice)                    # weights values restriction
		
		bias -= np.multiply(dB,lrate)
		bias = self.update_bias(bias, bias_choice)                               # biases values restriction
	   
		W = weights                                                             # assign values of weights and biases to...
		B = bias                                                                # ...matrices W and B for proper main()...
											# ...call after 1st loop is executed
		return A2, W, B, av_l
