# necessary standard library imports
import numpy as np

# Definition of three possibles ways for input presentation during training

def standardbatch(input_data, target_output):
	""" Create the standard batch with serially presented inputs
		in all epochs. """
	# null function that only serves for consistency in main() execution
	return input_data, target_output


def shuffledbatch(input_data, target_output):
	""" Create a batch with random order of unique inputs for every epoch.
		The target output is altered respectively. """
	temp = np.concatenate((input_data,target_output),axis=1)
	np.random.shuffle(temp)
	input_data = temp[:,0:-1]
	target_output = temp[:,-1:]
	
	return input_data, target_output

	
def randombatch(input_data, target_output):
	""" Create a batch with random inputs for every epoch. The target output
		is altered respectively. """
	selections = np.random.randint(0,len(input_data),size=len(input_data))
	input_data = input_data[selections]
	target_output = target_output[selections]
	
	return input_data, target_output

	
def call_batch(input_data, target_output, input_pres_choice):
	""" Run main() with the chosen input presentation method. """
	if input_pres_choice == 1:
		Xnew, Ynew = standardbatch(input_data,target_output)
	elif input_pres_choice == 2:
		Xnew, Ynew = shuffledbatch(input_data,target_output)
	elif input_pres_choice == 3:
		Xnew, Ynew = randombatch(input_data,target_output)
	
	return Xnew, Ynew
