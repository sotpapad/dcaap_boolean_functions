# Loop over the karyotypes (12 in total)
for y in $(seq 0.0 1.0 11.0); do

	export HPARAMS="${y}"

	qsub -v "HPARAMS=$HPARAMS" fig3_submit_script.sh

done
