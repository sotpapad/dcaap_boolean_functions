# necessary standard library imports
import numpy as np

# Definition of the dendritic and somatic activation functions

# Dendritic activation functions and derivatives are centered at 0.5, because the
# maximum outcome should coincide with the activation of 1 out of 2 binary inputs
# in the case of the non-monotonic functions
# decay constant defined experimentally to be 0.3 (Gidon 2019, figure 3.A)

# If the sigmoid is used as the dendritic activation function then it is
# centered at 0.6 in order to make it analogous to the results of Eyal (2018) -
# scaled version in [0,1] of AMPA+NMDA (figure 5.E)
# Since a definite outcome must be returned given the decision boundary of 0.5
# the sigmoid is made very steep (this matches the Eyal model too)

# Somatic activation function and derivative center is optional. Default setting
# at 0.0

# Slope is only needed for sigmoid(), but is passed in all functions for
# consistency purposes

# dCaAP activation function and derivative
def dCaAP(x,center=0.5):
	""" Difference of exponentials that closely resembles the experimental
		data for the dendritic activation function. Outputs >0.5 in the range
		[0.4725, 0.70]. """
	func_array = np.empty([x.shape[0],1])
	
	for index, item in enumerate(x):
		if item < 0.4725:
			func_array[index] = 0.0
		else:
			func_array[index] = np.exp( (-item + center)/0.3 ) - \
								np.exp(-210*(item-0.47))
	
	return func_array

def dCaAP_der(x,center=0.5):
	""" Derivative w.r.t. x, of the dCaAP activation function. """
	der_array = np.empty([x.shape[0],1])
	
	for index, item in enumerate(x):
		if item < 0.4725:
			der_array[index] = 0.0
		else: 
			der_array[index] = 210*np.exp(-210*(item-0.47)) - \
								10/3*np.exp(10*(center-item)/3)
								
	return der_array


# Gaussian activation function and derivative
def gaussian(x,center=0.5):
	""" Gaussian activation function; used to validate dCaAP, because of their
		resemblance (both are non-monotonic). Outputs >0.5 in the range
		[0.30, 0.70]."""
	return np.exp(-(x-center)**2/0.063)


def gaussian_der(x,center=0.5):
	""" Derivative of the gaussian activation function. """
	return -2000/63*(x-center)*np.exp(-1000/63*(x-center)**2)


# Sigmoid
def sigmoid(x,slope=25.0,center=0.0):
	""" Sigmoidal somatic activation funaction. Outputs >0.5 for x>0.5. """
	return 1./( 1 + np.exp(slope*(-x+center)) )


def sigmoid_der(x,slope=25.0,center=0.0):
	""" Derivative of sigmoidal activation function. """
	return steep*np.exp(slope*(center-x)) / ( np.exp(slope*(center-x)) + 1 )**2
