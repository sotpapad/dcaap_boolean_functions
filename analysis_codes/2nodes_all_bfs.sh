# Sleep duration between b2 loop
sleeptime=12m

# Loop over the Boolean Functions (order as defined in main_cluster.py)
#for y in $(seq 0.0 1.0 11.0); do
y=0.0

# Choose dendritic activation function:
# (1)dCaAP/(2)sigmoid/(3)gaussian
#for dendritic_function in $(seq 1.0 1.0 2.0); do
dendritic_function=1.0

# Choose the number of nodes for the simulation:
# (1)one node/(2)two nodes
nodes_choice=2.0

# Choose whether to impose restrictions on the weights or not:
# (1)Wis>0/(2)Wis<0/(3)One pos-One neg/(4)unrestricted weights
weights_choice=4.0

# Choose whether to impose restrictions on the bias(es) or not:
# (1)unrestricted bias(es)/(2)b1>0/(3)b1<0/(4)biases>0/(5)biases<0
bias_choice=1.0

# Choose whether to use regularization or not:
# (0)No/(1)Yes
regul_choice=0.0

# Choose how the input should be presented:
# (1)serial batch/(2)shuffled batch/(3)random batch
input_pres_choice=2.0

# Choose if the loss will be calculated as an average over 4 or 1:
# (1)vectorized/(2)loop
method_choice=1.0

# Choose whether to allow early code termination or not
# (0)No/(1)Yes
early_stop=0.0

# Make all the initializations for the one-node case

# null b2 value is passed for consistency
for b2 in $(seq -0.5 0.2 1.5); do

for w11 in $(seq -0.5 0.1 1.5); do
for w12 in $(seq -0.5 0.1 1.5); do
for b1 in $(seq -0.5 0.1 1.5); do

	export HPARAMS="${y} ${dendritic_function} ${nodes_choice} ${weights_choice} \n
					${bias_choice} ${regul_choice} ${input_pres_choice} \n
					${method_choice} ${early_stop} ${w11} ${w12} ${b1} ${b2}"

	qsub -v "HPARAMS=$HPARAMS" 2nodes_submit_script.sh

done
done
done

echo "Finished with b2 iteration: $y"
echo "Now going to sleep for $sleeptime"

sleep $sleeptime

done
