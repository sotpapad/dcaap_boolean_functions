########
#   ---NOTES---
#	0. Code that simulates a dendrite and/or neuron as a small network with one
#		or two nodes and trains it to learn any Boolean Function
#   1. Should run seamlessly in both python 2.x and 3.x
#   2. All supporting functions are defined so that they can support both
#       vectorized and for-loop methods
########

# import modules that resolve major python 2.x/3.x inconsistencies
from __future__ import generators, print_function
from six.moves import input

# standard library imports
import numpy as np
import os
from sys import argv

# custom, local files imports
import filename_generation
import run_method_test

################################################################################
# Run with predefined variables' values (from run_sims.sh)

# choice of Boolean Function through Y dictionary
y = int(11.0)								# 0-11

dendritic_function = 1.							# 1-3
nodes_choice = 1.0							# 1-2
weights_choice = 4.0							# 1-5
bias_choice = 1.0       						# 1-5
regul_choice = 0.0							# 0-1
input_pres_choice = 2.0							# 1-3
method_choice = 1.0							# 1-2
early_stop = 0.0							# 0-1

# Initialization values for weights and biases
w11 = np.array([0.3])
w12 = np.array([-0.2])
b1 = np.array([0.8])
#b2 = np.array([0.5])

################################################################################
# Hyper-parameters initialization

# Definition of the input data (X) and the target output (Y) for each input pattern
X = np.array([ [0.0, 0.0], [0.0, 1.0], [1.0, 0.0], [1.0, 1.0] ])

Ynames = ["xor", "xnor", "and", "nand", "or", "nor", "true", "false", "f4", "f5",
			"f12", "f13"]

Y = { "xor": np.array([0.0, 1.0, 1.0, 0.0]).reshape(-1,1),
		"xnor": np.array([1.0, 0.0, 0.0, 1.0]).reshape(-1,1),
		"and": np.array([0.0, 0.0, 0.0, 1.0]).reshape(-1,1),
		"nand": np.array([1.0, 1.0, 1.0, 0.0]).reshape(-1,1),
		"or": np.array([0.0, 1.0, 1.0, 1.0]).reshape(-1,1),
		"nor": np.array([1.0, 0.0, 0.0, 0.0]).reshape(-1,1),
		"true": np.array([1.0, 1.0, 1.0, 1.0]).reshape(-1,1),
		"false": np.array([0.0, 0.0, 0.0, 0.0]).reshape(-1,1),
		"f4": np.array([0.0, 1.0, 0.0, 0.0]).reshape(-1,1),
		"f5": np.array([0.0, 1.0, 0.0, 1.0]).reshape(-1,1),
		"f12": np.array([1.0, 1.0, 0.0, 0.0]).reshape(-1,1),
		"f13": np.array([1.0, 1.0, 0.0, 1.0]).reshape(-1,1) }

# Weights and Bias initialization in apropos to the number of nodes used
if nodes_choice == 1.0:
	W = np.array([w11, w12]).reshape(-1,1)
	B = np.array([b1]).reshape(-1,1)
	w_arr = str(np.array([w11, w12]))
	b_arr = str(np.array([b1]))
elif nodes_choice == 2.0:
	# w[2] derived from the experimental results that give a length constant
	# λ=195 μm (Gidon, Figures S1, D2)
	w2 = 0.21
	W = np.array([w11, w12, w2]).reshape(-1,1)
	B = np.array([b1, b2]).reshape(-1,1)
	w_arr = str(np.array([w11, w12, w2]))
	b_arr = str(np.array([b1, b2]))

# Set the number of max_epochs and create a variable that stores the number of
# epochs that the algorithm needs to converge, if an early stop is desired
max_epochs = 10000
epochs_el = 0                                                                   # epochs elapsed

# Learning rate initialization
lrate = 0.001                                                                   # seems to be the best value

# Regularization term lambda initialization
# Values of λs that practitioners recommend are:
# 0.02, 0.04, 0.08, 0.16, 0.32, 0.64, 1.28, 2.56, 5.12, 10.24
lamda = 0.08                                                                    # seems to be the best value

# Slope and center of sigmoid if a soma is included; center for the sigmoid
# activation function can be overridden, especially in the case we want to use
# it at the dendrite (see dendritic_functions.py)
slope = 25
centers = np.array([0.1, 0.6])                                                  # soma and dendrite (if sigmoid)

# Initialization of two arrays that store all computed outputs throughout epochs
# and their respective target output Y
allA2 = np.empty( [max_epochs, Y["nand"].shape[0]] )
allY = np.empty( [max_epochs, Y["nand"].shape[0]] )

# Initialization of array that stores the accuracy of the actual network output
# throughout epochs
acc = np.empty( [max_epochs, 1] )

# Initialization of array that stores all the computed average losses
av_L = np.empty( [max_epochs, 1] )

# Initialization of two arrays that store all the weights and biases values
# throughout epochs
w_val = np.empty( [max_epochs, W.shape[0]] )
b_val = np.empty( [max_epochs, B.shape[0]] )

################################################################################
# Main loop
if __name__== "__main__":
		
	# check the number of nodes	 
	if nodes_choice == 1.0:
		node_str = "1node"
	elif nodes_choice == 2.0:
		node_str = "2nodes"

	# text generation for directory and files creation
	w_str, b_str, act_func_str, reg_str, method_str, input_str = \
	filename_generation.filename_generation(weights_choice, bias_choice,
				dendritic_function, regul_choice, method_choice, input_pres_choice)
	
	# directory name generation
	dir_path = "../data/" + node_str + "_" + act_func_str + "_" + reg_str + \
				"/" + Ynames[y]
	# bypass OSError (FileExistsError)
	if not os.path.exists(dir_path):
		os.system('mkdir -m 775 -p ' + dir_path)
	
	# filenames generation
	name_list = [w_str, str(w_arr), b_str, str(b_arr), input_str, method_str]
	file_name = '!'.join(name_list)
	file_name = dir_path + '/' + file_name

	# call run_method.py	
	if method_choice == 1.0:
		# run in vectorized form
		epochs_el = run_method_test.vectorized(X, Y, Ynames[y], W, B, lrate, slope, centers,
			max_epochs, epochs_el, lamda, acc, allA2, allY, av_L, w_val, b_val,
			input_pres_choice, nodes_choice, weights_choice, bias_choice,
			dendritic_function, method_choice, regul_choice, early_stop)
			
	elif method_choice == 2.0:
		# run in "for loop" form
		epochs_el = run_method_test.loop(X, Y, Ynames[y], W, B, lrate, slope, centers,
			max_epochs, epochs_el, lamda, acc, allA2, allY, av_L, w_val, b_val,
			input_pres_choice, nodes_choice, weights_choice, bias_choice,
			dendritic_function, method_choice, regul_choice, early_stop)

	# save the file
	#~ np.savez(file_name, target_output=allY[0:epochs_el], output=allA2[0:epochs_el],
				#~ av_loss=av_L[0:epochs_el], weights=w_val[0:epochs_el],
				#~ bias=b_val[0:epochs_el], accuracy=acc[0:epochs_el])
