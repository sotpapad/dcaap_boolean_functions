# do not send email (default)
#$ -m n

#Maximum walltime for this job
#$ -l h_rt=9999:00:00
#Maximum cpu time for this job
#$ -l h_cpu=9999:00:00

# Specify the shell to use when running the job script
#$ -S /bin/sh

# Directory to perform the job
#$ -cwd

# Name of the Job
#$ -N figure_3_txt_files_generation

#$ -o /home/cluster/sotirisp/all_bfs/log_files/out3
#$ -e /home/cluster/sotirisp/all_bfs/log_files/err3

# run the program
/home/cluster/applications/anaconda3/bin/python3 karyotypes_generation.py $HPARAMS
