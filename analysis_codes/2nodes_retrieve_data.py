#! python3

import os
import numpy as np

################################################################################
# Creation of dictionary that contains the names and corresponding paths of all
# 12 Boolean Functions
bf_paths = {"XOR": "/home/cluster/sotirisp/all_bfs/data/2nodes_dCaAP_noreg/xor",
			"XNOR": "/home/cluster/sotirisp/all_bfs/data/2nodes_dCaAP_noreg/xnor",
			"AND": "/home/cluster/sotirisp/all_bfs/data/2nodes_dCaAP_noreg/and",
			"NAND": "/home/cluster/sotirisp/all_bfs/data/2nodes_dCaAP_noreg/nand",
			"OR": "/home/cluster/sotirisp/all_bfs/data/2nodes_dCaAP_noreg/or",
			"NOR": "/home/cluster/sotirisp/all_bfs/data/2nodes_dCaAP_noreg/nor"}

################################################################################
# Create 3D matrix to keep the data
epochs = 9999
initializations = 9261*11
# final values matrix
M = np.empty([epochs*initializations,5])

# Initiate counts for correct data insertion in each array
matrix_count = 0

# Load the saved data (1 logical operation per time)
for bf_name, bf_path in bf_paths.items():
	# data path
	basepath = bf_path
	directory = os.listdir(basepath)

	for file in directory:
		# create full path
		filename = basepath + "/" + file
		data = np.load(filename)

		# retrieve all weights, bias, accuracy throughout epochs
		weights = data["weights"]
		bias = data["bias"]
		accuracy = data["accuracy"]

		# insert the values into the matrix
		M[epochs*matrix_count:epochs*(matrix_count+1),0:2] = weights[:]
		M[epochs*matrix_count:epochs*(matrix_count+1),2:4] = bias[:]
		M[epochs*matrix_count:epochs*(matrix_count+1),4:5] = accuracy[:]

		matrix_count += 1

	# move to the next logical operation and reset matrix_count
	matrix_count = 0
	
################################################################################
	# Save the results of the above process, so that it is not repeated each time
	save_path = "/home/cluster/sotirisp/all_bfs/data/2nodes_" + bf_name.lower()
	np.savez_compressed(save_path, sols_matrix=M)
