# necessary standard library imports
import numpy as np

# Definition of function that estimates the accuracy of the main algorithm in each
# epoch; it takes into account the per-input accuracy, in case inputs are
# presented one at time or as random batches, rather than in a standard batch that
# contains all 4 different inputs

def countacc(input_data, output_name, output):
	""" Count the percentage of correct outputs per epoch. """
	
	# set the thresholds that will evaluate a function as correctly computed
	h_thres = 0.55
	l_thres = 0.45
	
	# count for accuracy in each epoch - max is 4(/4)
	count = 0.0

	# evaluate output for a given Boolean Function
	for index, item in enumerate(input_data):
		if output_name == "xor":
			if np.array_equal(item,[0.0, 0.0]) and output[index] <= l_thres:
				count += 1
			elif np.array_equal(item,[0.0, 1.0]) and output[index] >= h_thres:
				count += 1
			elif np.array_equal(item,[1.0, 0.0]) and output[index] >= h_thres:
				count += 1
			elif np.array_equal(item,[1.0, 1.0]) and output[index] <= l_thres:
				count += 1
	
		elif output_name == "xnor":
			if np.array_equal(item,[0.0, 0.0]) and output[index] >= h_thres:
				count += 1
			elif np.array_equal(item,[0.0, 1.0]) and output[index] <= l_thres:
				count += 1
			elif np.array_equal(item,[1.0, 0.0]) and output[index] <= l_thres:
				count += 1
			elif np.array_equal(item,[1.0, 1.0]) and output[index] >= h_thres:
				count += 1
				
		elif output_name == "and":
			if np.array_equal(item,[0.0, 0.0]) and output[index] <= l_thres:
				count += 1
			elif np.array_equal(item,[0.0, 1.0]) and output[index] <= l_thres:
				count += 1
			elif np.array_equal(item,[1.0, 0.0]) and output[index] <= l_thres:
				count += 1
			elif np.array_equal(item,[1.0, 1.0]) and output[index] >= h_thres:
				count += 1
				
		elif output_name == "nand":
			if np.array_equal(item,[0.0, 0.0]) and output[index] >= h_thres:
				count += 1
			elif np.array_equal(item,[0.0, 1.0]) and output[index] >= h_thres:
				count += 1
			elif np.array_equal(item,[1.0, 0.0]) and output[index] >= h_thres:
				count += 1
			elif np.array_equal(item,[1.0, 1.0]) and output[index] <= l_thres:
				count += 1
				
		elif output_name == "or":
			if np.array_equal(item,[0.0, 0.0]) and output[index] <= l_thres:
				count += 1
			elif np.array_equal(item,[0.0, 1.0]) and output[index] >= h_thres:
				count += 1
			elif np.array_equal(item,[1.0, 0.0]) and output[index] >= h_thres:
				count += 1
			elif np.array_equal(item,[1.0, 1.0]) and output[index] >= h_thres:
				count += 1
				
		elif output_name == "nor":
			if np.array_equal(item,[0.0, 0.0]) and output[index] >= h_thres:
				count += 1
			elif np.array_equal(item,[0.0, 1.0]) and output[index] <= l_thres:
				count += 1
			elif np.array_equal(item,[1.0, 0.0]) and output[index] <= l_thres:
				count += 1
			elif np.array_equal(item,[1.0, 1.0]) and output[index] <= l_thres:
				count += 1
				
		elif output_name == "true":
			if np.array_equal(item,[0.0, 0.0]) and output[index] >= h_thres:
				count += 1
			elif np.array_equal(item,[0.0, 1.0]) and output[index] >= h_thres:
				count += 1
			elif np.array_equal(item,[1.0, 0.0]) and output[index] >= h_thres:
				count += 1
			elif np.array_equal(item,[1.0, 1.0]) and output[index] >= h_thres:
				count += 1
				
		elif output_name == "false":
			if np.array_equal(item,[0.0, 0.0]) and output[index] <= l_thres:
				count += 1
			elif np.array_equal(item,[0.0, 1.0]) and output[index] <= l_thres:
				count += 1
			elif np.array_equal(item,[1.0, 0.0]) and output[index] <= l_thres:
				count += 1
			elif np.array_equal(item,[1.0, 1.0]) and output[index] <= l_thres:
				count += 1
				
		elif output_name == "f4":
			if np.array_equal(item,[0.0, 0.0]) and output[index] <= l_thres:
				count += 1
			elif np.array_equal(item,[0.0, 1.0]) and output[index] >= h_thres:
				count += 1
			elif np.array_equal(item,[1.0, 0.0]) and output[index] <= l_thres:
				count += 1
			elif np.array_equal(item,[1.0, 1.0]) and output[index] <= l_thres:
				count += 1
				
		elif output_name == "f5":
			if np.array_equal(item,[0.0, 0.0]) and output[index] <= l_thres:
				count += 1
			elif np.array_equal(item,[0.0, 1.0]) and output[index] >= h_thres:
				count += 1
			elif np.array_equal(item,[1.0, 0.0]) and output[index] <= l_thres:
				count += 1
			elif np.array_equal(item,[1.0, 1.0]) and output[index] >= h_thres:
				count += 1
				
		elif output_name == "f12":
			if np.array_equal(item,[0.0, 0.0]) and output[index] >= h_thres:
				count += 1
			elif np.array_equal(item,[0.0, 1.0]) and output[index] >= h_thres:
				count += 1
			elif np.array_equal(item,[1.0, 0.0]) and output[index] <= l_thres:
				count += 1
			elif np.array_equal(item,[1.0, 1.0]) and output[index] <= l_thres:
				count += 1
				
		elif output_name == "f13":
			if np.array_equal(item,[0.0, 0.0]) and output[index] >= h_thres:
				count += 1
			elif np.array_equal(item,[0.0, 1.0]) and output[index] >= h_thres:
				count += 1
			elif np.array_equal(item,[1.0, 0.0]) and output[index] <= l_thres:
				count += 1
			elif np.array_equal(item,[1.0, 1.0]) and output[index] >= h_thres:
				count += 1
	
	# correct outputs / 4 * 100%
	accuracy = count/len(input_data)*100
	return accuracy
