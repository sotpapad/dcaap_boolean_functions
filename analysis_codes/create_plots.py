# Function for plotting variables throughout one code run

import matplotlib.pyplot as plt

def create_plots(input_data, max_epochs, av_loss, acc_in_ep,
				 weights, bias, weights_change, bias_change):
	""" Create necessary plots for visualization. Code written during early 
		development and initial testing. """
	# Plot average loss and individual losses
	# set the epochs range
	rng = range(1,max_epochs+1)
	
	fig1 = plt.figure()
	
	# average loss (av_L) subplot
	ax11 = fig1.add_subplot(111)
	ax11.set_ylabel("Loss Amplitude")
	ax11.set_title("Average loss throughout epochs")
	ax11.plot(rng,av_loss[0:max_epochs])
	plt.grid(True)
	
	# individual losses (in_L) subplot
	#~ ax12 = fig1.add_subplot(212)
	#~ ax12.set_xlabel("Number of Epochs")
	#~ ax12.set_ylabel("Loss Amplitude")
	#~ ax12.set_title("Per-input losses throughout epochs")
	#~ ax12.plot(rng,ind_loss[0:max_epochs])
	#~ ax12.legend(X[:,0:len(input_data)], title="Presented input", fontsize="small")
	#~ plt.grid(True)
	
	plt.tight_layout()
	
	# Plot total accuracy
	fig2 = plt.figure()
	
	# total accuracy subplot
	ax21 = fig2.add_subplot(111)
	ax21.set_xlabel("Number of Epochs")
	ax21.set_ylabel("Accuracy (%)")
	ax21.set_ylim(0,105)
	ax21.set_yticks([0, 25, 50, 75, 100])
	ax21.set_title("Accuracy throughout epochs")
	ax21.scatter(rng,(acc_in_ep[0:max_epochs]),s=10)
	plt.grid(True)
	
	# Plot weights and biases values throughout epochs
	fig3 = plt.figure()
	
	# weights subplot
	ax31 = fig3.add_subplot(211)
	ax31.set_ylabel("Weights Values")
	ax31.set_title("Weights values throughout epochs")
	ax31.plot(rng,weights_change[0:max_epochs])
	ax31.legend(weights[:,0], title="Weights", fontsize="small")
	plt.grid(True)
	
	# biases subplot
	ax32 = fig3.add_subplot(212)
	ax32.set_ylabel("Biases Values")
	ax32.set_xlabel("Number of Epochs")
	ax32.set_title("Biases values throughout epochs")
	ax32.plot(rng,bias_change[0:max_epochs])
	ax32.legend(bias[:,0], title="Biases", fontsize="small")
	plt.grid(True)
	
	plt.tight_layout()
	
	# Plot the presented inputs
	#~ fig4 = plt.figure()
	#~ objects = ("[0,0]","[0,1]","[1,0]","[1,1]")

	#~ ax41 = fig4.add_subplot(111)
	#~ x_pos = np.arange(len(objects))
	#~ count = [c0,c1,c2,c3]
	#~ ax41.bar(x_pos,count,align="center",alpha=0.5)
	#~ ax41.set_xticks(x_pos)
	#~ ax41.set_xticklabels(objects)
	#~ ax41.set_xlabel("Inputs")
	#~ ax41.set_ylabel("Number of presentations")
	#~ ax41.set_title("Number of distinct inputs presentations throughout training")

	# Create the Sup. Figure 1
	fig5 = plt.figure()

	ax51 = fig5.add_subplot(411)
	ax51.set_ylabel("$W_{11}$")
	ax51.spines['top'].set_visible(False)
	ax51.spines['right'].set_visible(False)
	ax51.plot(rng,weights_change[0:max_epochs,0])
	plt.grid(True)

	ax52 = fig5.add_subplot(412)
	ax52.set_ylabel("$W_{12}$")
	ax52.spines['top'].set_visible(False)
	ax52.spines['right'].set_visible(False)
	ax52.plot(rng,weights_change[0:max_epochs,1])
	plt.grid(True)
 
	ax53 = fig5.add_subplot(413)
	ax53.set_ylabel("$b_1$")
	ax53.spines['top'].set_visible(False)
	ax53.spines['right'].set_visible(False)
	ax53.plot(rng,bias_change[0:max_epochs,0])
	plt.grid(True)
	
	# average loss (av_L) subplot
	ax54 = fig5.add_subplot(414)
	ax54.set_ylabel("Loss Amplitude")
	ax54.set_xlabel("Epochs")
	ax54.spines['top'].set_visible(False)
	ax54.spines['right'].set_visible(False)
	ax54.plot(rng,av_loss[0:max_epochs])
	plt.grid(True)
	
	#plt.tight_layout()

	plt.show()
